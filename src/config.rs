#[derive(Debug, PartialEq)]
pub struct Config {
    pub query: String,
    pub case_sensitive: bool,
    pub filename: Option<String>,
}
