#[derive(PartialEq)]
enum State { Leading, Text(char), Trailing(char) }

// > A setext heading underline is a sequence of '=' characters or a sequence
// > of '-' characters, with no more than 3 spaces of indentation and any
// > number of trailing spaces or tabs.
//
// This would be a heck of a lot easier with the regex crate.
pub fn setext_level(content: &String) -> Option<usize> {
    let mut state = State::Leading;

    for (index, character) in content.chars().enumerate() {
        match state {
            State::Leading => {
                if character == ' ' && index < 3 {
                    continue;
                }
                if character == '=' || character == '-' {
                    state = State::Text(character);
                } else {
                    return None;
                }
            }
            State::Text(previous) => {
                if character.is_ascii_whitespace() {
                    state = State::Trailing(previous);
                } else if character != previous {
                    return None;
                }
            }
            State::Trailing(_) => {
                if !character.is_ascii_whitespace() {
                    return None;
                }
            }
        }
    }

    match state {
        State::Leading => None,
        State::Text(char) | State::Trailing(char) => {
            if char == '=' {
                Some(1)
            } else {
                Some(2)
            }
        }
    }
}

#[cfg(test)]
mod setext {
    use super::setext_level;

    fn run(text: &str) -> Option<usize> {
        setext_level(&String::from(text))
    }

    #[test]
    fn empty() {
        assert_eq!(run(""), None);
        assert_eq!(run(" "), None);
        assert_eq!(run("\t"), None);
        assert_eq!(run(" \t"), None);
        assert_eq!(run("\t "), None);
    }

    #[test]
    fn heading_one() {
        assert_eq!(run("="), Some(1));
        assert_eq!(run("=="), Some(1));
        assert_eq!(run("==="), Some(1));
        assert_eq!(run("===="), Some(1));
        assert_eq!(run("====="), Some(1));
    }

    #[test]
    fn leading_heading_one() {
        assert_eq!(run(" ="), Some(1));
        assert_eq!(run("  ="), Some(1));
        assert_eq!(run("   ="), Some(1));
        assert_eq!(run("    ="), None);
        assert_eq!(run("\t="), None);
        assert_eq!(run(" =="), Some(1));
        assert_eq!(run("  =="), Some(1));
        assert_eq!(run("   =="), Some(1));
        assert_eq!(run("    =="), None);
        assert_eq!(run("\t=="), None);
    }

    #[test]
    fn trailing_heading_one() {
        assert_eq!(run("= "), Some(1));
        assert_eq!(run("=  "), Some(1));
        assert_eq!(run("=   "), Some(1));
        assert_eq!(run("=    "), Some(1));
        assert_eq!(run("=\t"), Some(1));
        assert_eq!(run("=\t "), Some(1));
        assert_eq!(run("= \t"), Some(1));
        assert_eq!(run("= ="), None);
        assert_eq!(run("=\t="), None);
        assert_eq!(run("== "), Some(1));
        assert_eq!(run("==  "), Some(1));
        assert_eq!(run("==   "), Some(1));
        assert_eq!(run("==    "), Some(1));
        assert_eq!(run("==\t"), Some(1));
        assert_eq!(run("==\t "), Some(1));
        assert_eq!(run("== \t"), Some(1));
        assert_eq!(run("== ="), None);
        assert_eq!(run("==\t="), None);
    }

    #[test]
    fn heading_two() {
        assert_eq!(run("-"), Some(2));
        assert_eq!(run("--"), Some(2));
        assert_eq!(run("---"), Some(2));
        assert_eq!(run("----"), Some(2));
        assert_eq!(run("-----"), Some(2));
    }

    #[test]
    fn leading_heading_two() {
        assert_eq!(run(" -"), Some(2));
        assert_eq!(run("  -"), Some(2));
        assert_eq!(run("   -"), Some(2));
        assert_eq!(run("    -"), None);
        assert_eq!(run("\t-"), None);
        assert_eq!(run(" --"), Some(2));
        assert_eq!(run("  --"), Some(2));
        assert_eq!(run("   --"), Some(2));
        assert_eq!(run("    --"), None);
        assert_eq!(run("\t--"), None);
    }

    #[test]
    fn trailing_heading_two() {
        assert_eq!(run("- "), Some(2));
        assert_eq!(run("-  "), Some(2));
        assert_eq!(run("-   "), Some(2));
        assert_eq!(run("-    "), Some(2));
        assert_eq!(run("-\t"), Some(2));
        assert_eq!(run("-\t "), Some(2));
        assert_eq!(run("- \t"), Some(2));
        assert_eq!(run("- -"), None);
        assert_eq!(run("-\t-"), None);
        assert_eq!(run("-- "), Some(2));
        assert_eq!(run("--  "), Some(2));
        assert_eq!(run("--   "), Some(2));
        assert_eq!(run("--    "), Some(2));
        assert_eq!(run("--\t"), Some(2));
        assert_eq!(run("--\t "), Some(2));
        assert_eq!(run("-- \t"), Some(2));
        assert_eq!(run("-- -"), None);
        assert_eq!(run("--\t-"), None);
    }

    #[test]
    fn mixed() {
        assert_eq!(run("-="), None);
        assert_eq!(run("=-"), None);
        assert_eq!(run("-=-"), None);
        assert_eq!(run("=-="), None);
        assert_eq!(run(" -="), None);
        assert_eq!(run(" =-"), None);
        assert_eq!(run(" -=-"), None);
        assert_eq!(run(" =-="), None);
        assert_eq!(run("-= "), None);
        assert_eq!(run("=- "), None);
        assert_eq!(run("-=- "), None);
        assert_eq!(run("=-= "), None);
    }
}
