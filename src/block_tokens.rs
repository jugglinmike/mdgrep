use std::collections::VecDeque;
use crate::read_utf8::ReadUtf8;
use crate::infinite_peekable::InfinitePeekable;

const MAX_HEADING_LEVEL: usize = 6;
const TAB_STOP: usize = 4;
const MAX_LIST_START_DIGITS: usize = 9;

fn char_to_decimal(c: char) -> Option<usize> {
    match c.to_digit(10) {
        Some(number) => Some(number as usize),
        None => None,
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct ListItemMarker {
    pub kind: ListItemMarkerKind,
    // TODO: limit `space` to values 1 through 4
    pub space: usize,
}

#[derive(Clone, PartialEq, Debug)]
pub enum ListItemMarkerKind {
    Dash,
    Plus,
    Star,
    Integer(ListItemMarkerSeparator, usize),
}

impl ListItemMarker {
    pub fn from(c: char, space: usize) -> ListItemMarker {
        let kind = match c {
            '-' => ListItemMarkerKind::Dash,
            '+' => ListItemMarkerKind::Plus,
            '*' => ListItemMarkerKind::Star,
            _ => panic!("Unrecognized list item marker"),
        };

        ListItemMarker { kind, space }
    }

    pub fn from_number(separator: char, number: usize, space: usize) -> ListItemMarker {
        let kind = ListItemMarkerKind::Integer(
            ListItemMarkerSeparator::from(separator),
            number
        );

        ListItemMarker { kind, space }
    }

    pub fn width(&self) -> usize {
        let token = 1 + match self.kind {
            ListItemMarkerKind::Integer(_, num) => {
                1 + (num as f32).log10().floor() as usize
            },
            _ => 0,
        };

        token + self.space
    }

    pub fn matches(&self, other: &ListItemMarker) -> bool {
        use ListItemMarkerKind::Integer;

        match (&self.kind, &other.kind) {
            (Integer(self_separator, _), Integer(other_separator, _)) => {
                self_separator == other_separator
            },
            (_, _) => self.kind == other.kind
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum ListItemMarkerSeparator {
    Dot,
    Paren,
}

impl ListItemMarkerSeparator {
    pub fn from(c: char) -> ListItemMarkerSeparator {
        match c {
            '.' => ListItemMarkerSeparator::Dot,
            ')' => ListItemMarkerSeparator::Paren,
            _ => panic!("Unrecognized list item marker separator"),
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum TokenKind {
    ListItem(ListItemMarker),
    BlockQuote,
    Newline,
    Space,
    Heading(usize),
    CodeFence(CodeFence),
    Text,
}

#[derive(Clone, PartialEq, Debug)]
pub struct CodeFence {
    pub width: usize,
    pub info: Option<String>,
    pub marker: CodeFenceMarker,
}

impl CodeFence {
    pub fn closes(&self, opening: &CodeFence) -> bool {
        self.width >= opening.width &&
            self.marker == opening.marker &&
            self.info == None
    }
}

#[derive(Clone, PartialEq, Debug)]
pub enum CodeFenceMarker { Backtick, Tilde }

#[derive(PartialEq, Debug)]
pub struct Token {
    pub start: usize,
    pub source: String,
    pub kind: TokenKind,
}

impl Token {
    pub fn make_list_item(start: usize, source: String, marker: ListItemMarker) -> Token {
        Token { start, source, kind: TokenKind::ListItem(marker) }
    }

    pub fn make_block_quote(start: usize, source: String) -> Token {
        Token { start, source, kind: TokenKind::BlockQuote }
    }

    pub fn make_newline(start: usize, source: String) -> Token {
        Token { start, source, kind: TokenKind::Newline }
    }

    pub fn make_space(start: usize, source: String) -> Token {
        Token { start, source, kind: TokenKind::Space }
    }

    pub fn make_heading(start: usize, source: String, level: usize) -> Token {
        Token { start, source, kind: TokenKind::Heading(level) }
    }

    pub fn make_code_fence(start: usize, marker: CodeFenceMarker, info: Option<String>, width: usize, source: String) -> Token {
        let fence = CodeFence { width, info, marker };
        Token { start, source, kind: TokenKind::CodeFence(fence) }
    }

    pub fn make_text(start: usize, source: String) -> Token {
        Token { start, source, kind: TokenKind::Text }
    }
}

type Unwrapper = fn(v: Result<char, std::io::Error>) -> char;

pub struct Tokens<T: std::io::Read> {
    iter: InfinitePeekable<std::iter::Enumerate<std::iter::Map<ReadUtf8<T>, Unwrapper>>>,
    container_sequence: bool,
    line_start: usize,
    stored: VecDeque<Token>,
}

impl<T: std::io::Read> Tokens<T> {
    pub fn new(source: T) -> Tokens<T> {
        fn unwrap(item: Result<char, std::io::Error>) -> char {
            item.unwrap()
        }
        let chars = ReadUtf8::new(source)
            .map(unwrap as Unwrapper)
            .enumerate();
        Tokens {
            iter: InfinitePeekable::new(chars),
            container_sequence: true,
            line_start: 0,
            stored: VecDeque::new()
        }
    }

    pub fn line(&mut self) -> Option<(usize, VecDeque<Token>)> {
        if self.iter.peek(0) == None {
            None
        } else {
            let mut line = VecDeque::new();
            let mut newline = None;

            while let Some(token) = self.next() {
                if token.kind == TokenKind::Newline {
                    newline = Some(token);
                    break;
                }
                line.push_back(token);
            }

            let start = match (line.front(), newline) {
                (Some(first_token), _) => first_token.start,
                (None, Some(newline_token)) => newline_token.start,
                (None, None) => return None,
            };
            Some((start, line))
        }
    }

    fn consume_code_fence(&mut self, current: (usize, char)) -> Option<Token> {
        let marker = match current.1 {
            '`' => CodeFenceMarker::Backtick,
            '~' => CodeFenceMarker::Tilde,
            _ => return None,
        };

        let mut source = current.1.to_string();
        let delimiter;

        delimiter = current.1;

        let mut count = 1;
        let mut info_text = String::new();

        while let Some(next) = self.iter.peek(source.len() - 1) {
            match next.1 {
                '\r' | '\n' => break,
                ' ' | '\t' => {
                    if info_text.len() > 0 {
                        info_text.push(next.1);
                    }
                },
                other => {
                    if info_text.len() > 0 {
                        info_text.push(other);
                    } else {
                        if other == delimiter {
                            count += 1;
                        } else {
                            info_text.push(other);
                        }
                    }
                }
            };
            source.push(next.1);
        }

        if count >= 3 {
            let info = if info_text.len() > 0 {
                Some(info_text.trim().to_string())
            } else {
                None
            };

            for _ in 0..(source.len() - 1) {
                self.iter.next();
            }
            Some(Token::make_code_fence(current.0, marker, info, count, source))
        } else {
            None
        }
    }

    fn consume_spaces(&mut self, string: &mut String) -> usize {
        let mut space_count = 0;

        while let Some(next) = self.iter.peek(0).filter(|(_, c)| c.is_ascii_whitespace()) {
            string.push(next.1);
            self.iter.next();
            space_count += 1;
        }

        space_count
    }

    fn collect_chars(&mut self, string: &mut String, count: usize) -> () {
        while string.len() < count {
            match self.iter.next() {
                Some((_, c)) => string.push(c),
                None => break,
            };
        }
    }
}

impl<T: std::io::Read> Iterator for Tokens<T> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(item) = self.stored.pop_front() {
            return Some(item);
        }
        let current = match self.iter.next() {
            None => return None,
            Some(current) => current,
        };

        if self.container_sequence {
            if current.1 == '>' {
                // The following space is optional but should be ignored if present.
                let mut source = current.1.to_string();
                if let Some(next) = self.iter.peek(0) {
                    if next.1 == ' ' {
                        source.push(next.1);
                        self.iter.next();
                    }
                }
                return Some(Token::make_block_quote(current.0, source));
            }
            if let '-' | '+' | '*' = current.1 {
                if let Some(next) = self.iter.peek(0) {
                    if next.1.is_ascii_whitespace() {
                        let mut source = format!("{}{}", current.1, next.1);
                        self.iter.next();

                        // TODO: interpret tab characters according to relative
                        // tab stops
                        while let Some(next) = self.iter.peek(0).filter(|(_, c)| c.is_ascii_whitespace()) {
                            source.push(next.1);
                            self.iter.next();
                        }

                        let marker = ListItemMarker::from(current.1, source.len() - 1);
                        return Some(Token::make_list_item(current.0, source, marker));
                    }
                }
            }

            if let Some(mut digit) = char_to_decimal(current.1) {
                let mut source = current.1.to_string();
                while let Some(next) = self.iter.peek(source.len() - 1) {
                    if source.len() == MAX_LIST_START_DIGITS {
                        break;
                    } else if let Some(d) = char_to_decimal(next.1) {
                        digit = digit * 10 + d;
                        source.push(next.1);
                    } else {
                        break;
                    }
                }

                let separator = match self.iter.peek(source.len() - 1) {
                    Some((_, '.')) => Some('.'),
                    Some((_, ')')) => Some(')'),
                    _ => None,
                };
                let space = self.iter.peek(source.len())
                    .filter(|(_, c)| c.is_ascii_whitespace())
                    .map(|(_, c)| *c);

                if let (Some(separator), Some(space)) = (separator, space) {
                    for _ in 0..source.len()+1 {
                        self.iter.next();
                    }
                    source.push(separator);
                    source.push(space);

                    // TODO: interpret tab characters according to relative tab
                    // stops
                    let space = 1 + self.consume_spaces(&mut source);

                    let marker = ListItemMarker::from_number(
                        separator, digit, space
                    );
                    return Some(
                        Token::make_list_item(current.0, source, marker)
                    );
                }
            }

            if let '#' = current.1 {
                let mut source = current.1.to_string();

                while let Some((_, '#')) = self.iter.peek(source.len() - 1) {
                    source.push('#');

                    if source.len() == MAX_HEADING_LEVEL {
                        break;
                    }
                }
                let level = source.len();

                let mut no_text = false;

                while let Some((_, c)) = self.iter.peek(source.len() - 1) {
                    if c == &'\n' {
                        no_text = source.len() == level;
                        break;
                    }
                    if !c.is_ascii_whitespace() {
                        break;
                    }
                    source.push(*c);
                }

                if source.len() > level || no_text || self.iter.peek(source.len() - 1) == None {
                    for _ in 0..source.len()-1 {
                        self.iter.next();
                    }

                    let mut index = 0;
                    let mut close_count = 0;
                    let mut has_leading_space = true;
                    loop {
                        let next1 = match self.iter.peek(index) {
                            None => break,
                            Some(next) => next.1,
                        };
                        let next2 = self.iter.peek(index +1).map(|(_, c)| c);
                        match (next1, next2) {
                            ('\n', _) => {
                                break;
                            },
                            ('#', _) => {
                                if has_leading_space {
                                    close_count += 1;
                                }
                            },
                            ('\\', Some('#')) => {
                                index += 1;
                                close_count = 0;
                            },
                            (c, _) => {
                                has_leading_space = c.is_ascii_whitespace();
                                if has_leading_space {
                                    close_count += 1;
                                } else {
                                    close_count = 0;
                                }
                            }
                        };
                        index += 1;
                    }

                    if close_count > 0 {
                        let mut string = String::new();
                        self.consume_spaces(&mut string);
                        self.collect_chars(&mut string, index - close_count);
                        self.stored.push_back(
                            Token::make_text(current.0 + source.len(), string)
                        );
                        for _ in 0..close_count {
                            self.iter.next();
                        }
                    }

                    self.container_sequence = false;

                    return Some(Token::make_heading(current.0, source, level));
                }
            }

            if let Some(token) = self.consume_code_fence(current) {
                return Some(token);
            }

            if current.1 == ' ' {
                return Some(Token::make_space(current.0, current.1.to_string()));
            }

            // > Tabs in lines are not expanded to spaces. However, in contexts
            // > where spaces help to define block structure, tabs behave as if
            // > they were replaced by spaces with a tab stop of 4 characters.
            if current.1 == '\t' {
                let from_start = current.0 - self.line_start;

                for _ in 0..(TAB_STOP - from_start % TAB_STOP - 1) {
                    self.stored.push_back(Token::make_space(current.0, ' '.to_string()));
                }

                return Some(Token::make_space(current.0, ' '.to_string()));
            }

            self.container_sequence = false;
        }

        if current.1 == '\n' {
            self.line_start = current.0 + 1;
            self.container_sequence = true;
            return Some(Token::make_newline(current.0, '\n'.to_string()));
        }

        if current.1 == '\r' {
            if let Some((_, '\n')) = self.iter.peek(0) {
                self.iter.next();
                self.line_start = current.0 + 2;
                self.container_sequence = true;
                return Some(Token::make_newline(current.0, "\r\n".to_string()));
            }
        }

        let mut string = if current.1 == '\\' && self.iter.peek(0).map(|(_, c)| c.is_ascii_punctuation()) == Some(true) {
            String::new()
        } else {
            String::from(current.1)
        };

        loop {
            match self.iter.peek(0) {
                Some((_, '\n' | '\r')) => break,
                Some((_, character)) => {
                    string.push(*character);
                    self.iter.next();
                },
                _ => break,
            }
        }

        Some(Token::make_text(current.0, string))
    }
}

pub fn count_spaces(tokens: &VecDeque<Token>) -> usize {
    tokens
        .iter()
        .take_while(|c| c.kind == TokenKind::Space)
        .count()
}

pub fn to_text(tokens: &VecDeque<Token>, ignore_spaces: usize) -> Option<Token> {
    let mut iter = tokens
        .iter()
        .enumerate()
        // Ignore leading spaces for code blocks that have been initiated by an
        // indented code fence (but only up to the size of the indentation).
        .skip_while(|(index, token)| index < &ignore_spaces && token.kind == TokenKind::Space)
        .peekable();

    let start = if let Some((_, front)) = iter.peek() {
        front.start
    } else {
        return None;
    };

    let text = iter
        .take_while(|(_, token)| token.kind != TokenKind::Newline)
        .fold(String::new(), |accumulator, (_, token)| format!("{}{}", accumulator, token.source));
    Some(Token::make_text(start, text))
}

#[cfg(test)]
mod block_tokens {
    use super::*;
    use ListItemMarkerKind::*;
    use ListItemMarkerSeparator::*;

    fn t_list_item(index: usize, slice: &str, kind: ListItemMarkerKind, space: usize) -> Token {
        Token::make_list_item(index, slice.to_string(), ListItemMarker { space, kind })
    }
    fn t_block_quote(index: usize, slice: &str) -> Token {
        Token::make_block_quote(index, slice.to_string())
    }
    fn t_newline(index: usize, slice: &str) -> Token {
        Token::make_newline(index, slice.to_string())
    }
    fn t_space(index: usize, slice: &str) -> Token {
        Token::make_space(index, slice.to_string())
    }
    fn t_heading(index: usize, slice: &str, level: usize) -> Token {
        Token::make_heading(index, slice.to_string(), level)
    }

    fn doit(source: &'static str) -> Vec<Token> {
        Tokens::new(std::io::Cursor::new(source)).collect::<Vec<Token>>()
    }

    fn t(index: usize, slice: &str) -> Token {
        Token::make_text(index, slice.to_string())
    }

    #[test]
    fn single_line() {
        assert_eq!(doit("a b c"), [t(0, "a b c")]);
    }

    #[test]
    fn newline_unix() {
        assert_eq!(doit("a\nb"), [t(0, "a"), t_newline(1, "\n"), t(2, "b")]);
    }

    #[test]
    fn newline_windows() {
        assert_eq!(doit("a\r\nb"), [t(0, "a"), t_newline(1, "\r\n"), t(3, "b")]);
    }

    #[test]
    fn space_initial() {
        assert_eq!(doit(" a"), [t_space(0, " "), t(1, "a")]);
        assert_eq!(doit("  a"), [t_space(0, " "), t_space(1, " "), t(2, "a")]);
        assert_eq!(doit("   a"), [t_space(0, " "), t_space(1, " "), t_space(2, " "), t(3, "a")]);
    }

    #[test]
    fn tab_stop() {
        assert_eq!(doit("\ta"), [t_space(0, " "), t_space(0, " "), t_space(0, " "), t_space(0, " "), t(1, "a")]);
        assert_eq!(doit(" \ta"), [t_space(0, " "), t_space(1, " "), t_space(1, " "), t_space(1, " "), t(2, "a")]);
        assert_eq!(doit("  \ta"), [t_space(0, " "), t_space(1, " "), t_space(2, " "), t_space(2, " "), t(3, "a")]);
        assert_eq!(doit("   \ta"), [t_space(0, " "), t_space(1, " "), t_space(2, " "), t_space(3, " "), t(4, "a")]);
        assert_eq!(
            doit("    \ta"),
            [t_space(0, " "), t_space(1, " "), t_space(2, " "), t_space(3, " "), t_space(4, " "), t_space(4, " "), t_space(4, " "), t_space(4, " "), t(5, "a")]
        );
        assert_eq!(
            doit("\n\ta"),
            [t_newline(0, "\n"), t_space(1, " "), t_space(1, " "), t_space(1, " "), t_space(1, " "), t(2, "a")]
        );
        assert_eq!(
            doit("\r\n\ta"),
            [t_newline(0, "\r\n"), t_space(2, " "), t_space(2, " "), t_space(2, " "), t_space(2, " "), t(3, "a")]
        );
        assert_eq!(
            doit("a\n\tb"),
            [t(0, "a"), t_newline(1, "\n"), t_space(2, " "), t_space(2, " "), t_space(2, " "), t_space(2, " "), t(3, "b")]
        );
        assert_eq!(
            doit("a\r\n\tb"),
            [t(0, "a"), t_newline(1, "\r\n"), t_space(3, " "), t_space(3, " "), t_space(3, " "), t_space(3, " "), t(4, "b")]
        );
    }

    #[test]
    fn blockquote_initial() {
        assert_eq!(doit(">a"), [t_block_quote(0, ">"), t(1, "a")]);
    }

    #[test]
    fn blockquote_start() {
        assert_eq!(doit("a\n>"), [t(0, "a"), t_newline(1, "\n"), t_block_quote(2, ">")]);
    }

    #[test]
    fn blockquote_space() {
        assert_eq!(doit(">  a"), [t_block_quote(0, "> "), t_space(2, " "), t(3, "a")]);
    }

    #[test]
    fn blockquote_blockquote() {
        assert_eq!(doit(">>a"), [t_block_quote(0, ">"), t_block_quote(1, ">"), t(2, "a")]);
    }

    #[test]
    fn blockquote_empty() {
        assert_eq!(doit(">\n>"), [t_block_quote(0, ">"), t_newline(1, "\n"), t_block_quote(2, ">")]);
    }

    #[test]
    fn blockquote_list() {
        assert_eq!(doit(">- a"), [t_block_quote(0, ">"), t_list_item(1, "- ", Dash, 1), t(3, "a")]);
        assert_eq!(doit(">+ a"), [t_block_quote(0, ">"), t_list_item(1, "+ ", Plus, 1), t(3, "a")]);
        assert_eq!(doit(">* a"), [t_block_quote(0, ">"), t_list_item(1, "* ", Star, 1), t(3, "a")]);
        assert_eq!(doit(">1. a"), [t_block_quote(0, ">"), t_list_item(1, "1. ", Integer(Dot, 1), 1), t(4, "a")]);
        assert_eq!(doit(">1) a"), [t_block_quote(0, ">"), t_list_item(1, "1) ", Integer(Paren, 1), 1), t(4, "a")]);
        assert_eq!(doit(">12. a"), [t_block_quote(0, ">"), t_list_item(1, "12. ", Integer(Dot, 12), 1), t(5, "a")]);
        assert_eq!(doit(">123. a"), [t_block_quote(0, ">"), t_list_item(1, "123. ", Integer(Dot, 123), 1), t(6, "a")]);
    }

    #[test]
    fn greater_than() {
        assert_eq!(doit("a > b"), [t(0, "a > b")]);
    }

    #[test]
    fn fake_list_initial() {
        assert_eq!(doit("-a"), [t(0, "-a")]);
        assert_eq!(doit("+a"), [t(0, "+a")]);
        assert_eq!(doit("*a"), [t(0, "*a")]);
        assert_eq!(doit("1a"), [t(0, "1a")]);
        assert_eq!(doit("1.a"), [t(0, "1.a")]);
        assert_eq!(doit("1)a"), [t(0, "1)a")]);
        assert_eq!(doit("12a"), [t(0, "12a")]);
        assert_eq!(doit("12.a"), [t(0, "12.a")]);
        assert_eq!(doit("12)a"), [t(0, "12)a")]);
    }

    #[test]
    fn fake_list_start() {
        assert_eq!(doit("a\n-"), [t(0, "a"), t_newline(1, "\n"), t(2, "-")]);
        assert_eq!(doit("a\n+"), [t(0, "a"), t_newline(1, "\n"), t(2, "+")]);
        assert_eq!(doit("a\n*"), [t(0, "a"), t_newline(1, "\n"), t(2, "*")]);
        assert_eq!(doit("a\n1"), [t(0, "a"), t_newline(1, "\n"), t(2, "1")]);
        assert_eq!(doit("a\n1."), [t(0, "a"), t_newline(1, "\n"), t(2, "1.")]);
        assert_eq!(doit("a\n1)"), [t(0, "a"), t_newline(1, "\n"), t(2, "1)")]);
        assert_eq!(doit("a\n12"), [t(0, "a"), t_newline(1, "\n"), t(2, "12")]);
        assert_eq!(doit("a\n12."), [t(0, "a"), t_newline(1, "\n"), t(2, "12.")]);
        assert_eq!(doit("a\n12)"), [t(0, "a"), t_newline(1, "\n"), t(2, "12)")]);
    }

    #[test]
    fn list_initial() {
        assert_eq!(doit("- a"), [t_list_item(0, "- ", Dash, 1), t(2, "a")]);
        assert_eq!(doit("+ a"), [t_list_item(0, "+ ", Plus, 1), t(2, "a")]);
        assert_eq!(doit("* a"), [t_list_item(0, "* ", Star, 1), t(2, "a")]);
        assert_eq!(doit("1. a"), [t_list_item(0, "1. ", Integer(Dot, 1), 1), t(3, "a")]);
        assert_eq!(doit("1) a"), [t_list_item(0, "1) ", Integer(Paren, 1), 1), t(3, "a")]);
        assert_eq!(doit("12. a"), [t_list_item(0, "12. ", Integer(Dot, 12), 1), t(4, "a")]);
        assert_eq!(doit("123. a"), [t_list_item(0, "123. ", Integer(Dot, 123), 1), t(5, "a")]);
    }

    #[test]
    fn list_start() {
        assert_eq!(doit("a\n- "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "- ", Dash, 1)]);
        assert_eq!(doit("a\n+ "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "+ ", Plus, 1)]);
        assert_eq!(doit("a\n* "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "* ", Star, 1)]);
        assert_eq!(doit("a\n1. "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "1. ", Integer(Dot, 1), 1)]);
        assert_eq!(doit("a\n1) "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "1) ", Integer(Paren, 1), 1)]);
        assert_eq!(doit("a\n12. "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "12. ", Integer(Dot, 12), 1)]);
        assert_eq!(doit("a\n123. "), [t(0, "a"), t_newline(1, "\n"), t_list_item(2, "123. ", Integer(Dot, 123), 1)]);
    }

    #[test]
    fn list_space() {
        assert_eq!(doit("-  a"), [t_list_item(0, "-  ", Dash, 2), t(3, "a")]);
        assert_eq!(doit("+  a"), [t_list_item(0, "+  ", Plus, 2), t(3, "a")]);
        assert_eq!(doit("*  a"), [t_list_item(0, "*  ", Star, 2), t(3, "a")]);
        assert_eq!(doit("1.  a"), [t_list_item(0, "1.  ", Integer(Dot, 1), 2), t(4, "a")]);
        assert_eq!(doit("1)  a"), [t_list_item(0, "1)  ", Integer(Paren, 1), 2), t(4, "a")]);
        assert_eq!(doit("12.  a"), [t_list_item(0, "12.  ", Integer(Dot, 12), 2), t(5, "a")]);
        assert_eq!(doit("123.  a"), [t_list_item(0, "123.  ", Integer(Dot, 123), 2), t(6, "a")]);
    }

    #[test]
    fn list_blockquote() {
        assert_eq!(doit("- >a"), [t_list_item(0, "- ", Dash, 1), t_block_quote(2, ">"), t(3, "a")]);
        assert_eq!(doit("+ >a"), [t_list_item(0, "+ ", Plus, 1), t_block_quote(2, ">"), t(3, "a")]);
        assert_eq!(doit("* >a"), [t_list_item(0, "* ", Star, 1), t_block_quote(2, ">"), t(3, "a")]);
        assert_eq!(doit("1. >a"), [t_list_item(0, "1. ", Integer(Dot, 1), 1), t_block_quote(3, ">"), t(4, "a")]);
        assert_eq!(doit("1) >a"), [t_list_item(0, "1) ", Integer(Paren, 1), 1), t_block_quote(3, ">"), t(4, "a")]);
        assert_eq!(doit("12. >a"), [t_list_item(0, "12. ", Integer(Dot, 12), 1), t_block_quote(4, ">"), t(5, "a")]);
        assert_eq!(doit("123. >a"), [t_list_item(0, "123. ", Integer(Dot, 123), 1), t_block_quote(5, ">"), t(6, "a")]);
    }

    #[test]
    fn list_list() {
        assert_eq!(doit("- - a"), [t_list_item(0, "- ", Dash, 1), t_list_item(2, "- ", Dash, 1), t(4, "a")]);
        assert_eq!(doit("+ + a"), [t_list_item(0, "+ ", Plus, 1), t_list_item(2, "+ ", Plus, 1), t(4, "a")]);
        assert_eq!(doit("* * a"), [t_list_item(0, "* ", Star, 1), t_list_item(2, "* ", Star, 1), t(4, "a")]);
        assert_eq!(doit("1. 1. a"), [t_list_item(0, "1. ", Integer(Dot, 1), 1), t_list_item(3, "1. ", Integer(Dot, 1), 1), t(6, "a")]);
        assert_eq!(doit("1) 1) a"), [t_list_item(0, "1) ", Integer(Paren, 1), 1), t_list_item(3, "1) ", Integer(Paren, 1), 1), t(6, "a")]);
        assert_eq!(doit("12. 12. a"), [t_list_item(0, "12. ", Integer(Dot, 12), 1), t_list_item(4, "12. ", Integer(Dot, 12), 1), t(8, "a")]);
        assert_eq!(doit("123. 123. a"), [t_list_item(0, "123. ", Integer(Dot, 123), 1), t_list_item(5, "123. ", Integer(Dot, 123), 1), t(10, "a")]);
    }

    #[test]
    fn very_large_integer() {
        assert_eq!(doit("999999999. a"), [t_list_item(0, "999999999. ", Integer(Dot, 999999999), 1), t(11, "a")]);
        assert_eq!(doit("1000000000. a"), [t(0, "1000000000. a")]);
    }

    #[test]
    fn heading() {
        assert_eq!(doit("#"), [t_heading(0, "#", 1)]);
        assert_eq!(doit("#\n"), [t_heading(0, "#", 1), t_newline(1, "\n")]);
        assert_eq!(doit("# one"), [t_heading(0, "# ", 1), t(2, "one")]);
        assert_eq!(doit("#\tone"), [t_heading(0, "#\t", 1), t(2, "one")]);
        assert_eq!(doit("#  one"), [t_heading(0, "#  ", 1), t(3, "one")]);
        assert_eq!(doit("#\t\tone"), [t_heading(0, "#\t\t", 1), t(3, "one")]);

        assert_eq!(doit("## two"), [t_heading(0, "## ", 2), t(3, "two")]);
        assert_eq!(doit("### three"), [t_heading(0, "### ", 3), t(4, "three")]);
        assert_eq!(doit("#### four"), [t_heading(0, "#### ", 4), t(5, "four")]);
        assert_eq!(doit("##### five"), [t_heading(0, "##### ", 5), t(6, "five")]);
        assert_eq!(doit("###### six"), [t_heading(0, "###### ", 6), t(7, "six")]);
    }

    #[test]
    fn heading_end() {
        assert_eq!(doit("# >not blockquote"), [t_heading(0, "# ", 1), t(2, ">not blockquote")]);
        assert_eq!(doit("# > not blockquote"), [t_heading(0, "# ", 1), t(2, "> not blockquote")]);
        assert_eq!(doit("# - not list"), [t_heading(0, "# ", 1), t(2, "- not list")]);
        assert_eq!(doit("# # not another heading"), [t_heading(0, "# ", 1), t(2, "# not another heading")]);
    }

    #[test]
    fn not_heading() {
        assert_eq!(doit("#one"), [t(0, "#one")]);
        assert_eq!(doit("a#one"), [t(0, "a#one")]);
        assert_eq!(doit("a #one"), [t(0, "a #one")]);
        assert_eq!(doit("####### seven"), [t(0, "####### seven")]);
    }
}
