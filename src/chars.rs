/*!
The `read_char` crate provides easy way to read `char` from any `Read` instance.
*/
#![deny(missing_docs)]

use std::io::Read;
use std::fmt::{Display,Formatter};
use smallvec::SmallVec;

/// The kind of error that can occur when reading char(s) from `Read`.
#[derive(Debug)]
pub enum Error {
    /// Encountered invalid byte sequence.
    NotAnUtf8(Vec<u8>),
    /// Underlying `Read` instance returned an error.
    Io(std::io::Error),
    /// End Of File was reached.
    EOF,
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            Error::EOF => write!(f, "end of file"),
            Error::NotAnUtf8(b) => write!(f, "invalid utf-8 sequence: {:?}", b),
            Error::Io(e) => write!(f, "i/o error: {}", e),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::Io(e) => Some(e),
            _ => None,
        }
    }
}

const MAX_UNICODE_LEN : usize = 6;

/// Reads next valid utf-8 encoded char form `r`.
#[inline]
pub fn read_next_char<R: Read>(r: &mut R) -> Result<char, Error> {
    let mut offset = 0;
    let mut buf = SmallVec::<[u8; MAX_UNICODE_LEN+1]>::new();
    buf.push(0);
    loop {
        debug_assert!(!buf.spilled());
        if buf.len() > MAX_UNICODE_LEN {
            return Err(Error::NotAnUtf8(buf.to_vec()));
        }
        match r.read(&mut buf[offset..offset+1]) {
            Err(e) => return Err(Error::Io(e)),
            Ok(0) => return Err(Error::EOF),
            Ok(_) => {},
        };
        match std::str::from_utf8(&buf) {
            Ok(s) => {
                return Ok(s.chars().next().unwrap());
            },
            Err(_) => {
                buf.push(0);
                offset += 1;
            }
        }
    }
}

/// Iterator that reads char(s) from `Read` instance.
pub struct ReadIter<R: Read> {
    r: R
}

impl<R: Read> ReadIter<R> {
    /// Returns new `ReadIter` instance that reads from `r`.
    pub fn new(r: R) -> Self {
        ReadIter{r}
    }
}

impl<R: Read> Iterator for ReadIter<R> {
    type Item = Result<char, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        match read_next_char(&mut self.r) {
            Err(Error::EOF) => None,
            e @ Err(_) => Some(e),
            c @ Ok(_) => Some(c),
        }
    }
}
