use std::ops::Range;
use super::markdown_events;

#[derive(Debug)]
pub struct SourceLine {
    pub location: usize,
    pub source: String,
}

pub struct Stanza {
    text: Vec<(usize, String)>,
}

impl Stanza {
    pub fn new(text: Vec<(usize, String)>) -> Self {
        Stanza { text }
    }

    fn translate(&self, index: usize) -> usize {
        let mut foo = index;
        for item in self.text.iter() {
            if item.1.len() >= foo {
                return item.0 + foo;
            }
            foo -= item.1.len() + 1;
        }
        panic!("Stanza::translate: Failed to translate local index {} into a valid stream index.", index);
    }

    pub fn search(&self, query: &str) -> Vec<Range<usize>> {
        self.text
            .iter()
            .map(|(_, string)| string.clone())
            .collect::<Vec<_>>()
            .join(" ")
            .match_indices(query)
            .map(|(index, slice)| {
                self.translate(index)..self.translate(index + slice.len())
            })
            .collect::<Vec<_>>()
    }
}

#[cfg(test)]
mod search {
    use super::*;

    fn s(items: Vec<(usize, &str)>) -> Stanza {
        Stanza::new(
            items.iter().map(|(loc, slice)| (*loc, slice.to_string())).collect()
        )
    }

    #[test]
    fn single_lone_beginning() {
        let stanza = s(vec![(1, "hey there")]);
        assert_eq!(stanza.search("hey"), vec![1..4]);
    }

    #[test]
    fn single_lone_middle() {
        let stanza = s(vec![(3, "hey there")]);
        assert_eq!(stanza.search("the"), vec![7..10]);
    }

    #[test]
    fn single_lone_end() {
        let stanza = s(vec![(2, "hey there")]);
        assert_eq!(stanza.search("there"), vec![6..11]);
    }

    #[test]
    fn single_lone_all() {
        let stanza = s(vec![(32, "hey there")]);
        assert_eq!(stanza.search("hey there"), vec![32..41]);
    }

    #[test]
    fn single_many() {
        let stanza = s(vec![(11, "hey there"), (22, "it's me"), (33, "jenna")]);
        assert_eq!(stanza.search("'s"), vec![24..26]);
    }

    #[test]
    fn across_two() {
        let stanza = s(vec![(11, "hey there"), (22, "it's me"), (33, "jenna")]);
        assert_eq!(stanza.search("me jenna"), vec![27..38]);
    }

    #[test]
    fn across_three() {
        let stanza = s(vec![(11, "hey there"), (22, "it's me"), (33, "jenna")]);
        assert_eq!(stanza.search("there it's me jen"), vec![15..36]);
    }
}

pub enum Stuff {
    Stanza(Stanza),
    SourceLine(SourceLine),
}

pub struct Stanzas<T: std::io::Read> {
    events: markdown_events::MarkdownEvents<T>,
    stanza_lines: Option<Vec<(usize, String)>>,
}

impl<T: std::io::Read> Stanzas<T> {
    pub fn new(events: markdown_events::MarkdownEvents<T>) -> Self {
        Self { events, stanza_lines: None }
    }
}

impl<T: std::io::Read> Iterator for Stanzas<T> {
    type Item = Stuff;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.events.next() {
                Some(markdown_events::Event::Text(loc, string)) => {
                    self.stanza_lines
                        .get_or_insert_with(|| vec![])
                        .push((loc, string));
                },
                Some(markdown_events::Event::SourceLine(loc, string)) => {
                    return Some(Stuff::SourceLine(SourceLine {
                        location: loc,
                        source: string,
                    }));
                },
                next => {
                    let stanza = self.stanza_lines
                        .take()
                        .map(|text| Stuff::Stanza(Stanza::new(text)));
                    if stanza.is_some() {
                        return stanza;
                    }
                    if next.is_none() {
                        return None;
                    }
                },
            }
        }
    }
}
