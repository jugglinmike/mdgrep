use super::config::Config;
use std::fmt;

#[derive(Debug, PartialEq)]
pub struct SearchResult {
    source: String,
    start: usize,
    end: usize,
}

impl SearchResult {
    pub fn new(source: String, start: usize, end: usize) -> Self {
        assert!(start < end, "start ({}) < end ({})", start, end);
        let source_length = source.len();
        assert!(end <= source_length, "end ({}) <= source_length ({})", end, source_length);
        SearchResult { source, start, end }
    }

    pub fn print(&self, config: &Config) -> String {
        let prelude = match &config.filename {
            Some(name) => format!("\x1b[36m{}:\x1b[m", name),
            None => String::new(),
        };
        let raw_lines = self.source.split("\n");
        let mut lines = vec![];
        let mut line_start = 0;
        for raw_line in raw_lines {
            let mut line = raw_line.to_string();
            let length = line.len();
            let line_end = line_start + length;

            let open_index = if self.start >= line_start && self.start < line_end {
                Some(self.start - line_start)

            // Re-open from a previous line
            } else if self.start < line_start && self.end > line_start {
                Some(0)
            } else { None };

            let close_index = if self.end >= line_start && self.end <= line_end {
                Some(self.end - line_start)

            // Close at the end of every line, regardless of match
            } else if open_index.is_some() {
                Some(line_end)
            } else { None };

            if let Some(close_index) = close_index {
                line.insert_str(close_index, "\x1b[m");
            }

            if let Some(open_index) = open_index {
                line.insert_str(open_index, "\x1b[31;1m");
            }

            // Offsets include newline characters
            line_start += length + 1;

            lines.push(format!("{}{}", prelude, line));
        }
        lines.join("\n")
    }
}

impl fmt::Display for SearchResult {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let before = &self.source[..self.start];
        let middle = &self.source[self.start..self.end];
        let after = &self.source[self.end..];

        write!(f, "{:?}||{:?}||{:?}", before, middle, after)
    }
}

#[cfg(test)]
mod print {
    use super::*;

    enum Filename<'a> { Omit, Include(&'a str) }

    fn make(text: &str, start: usize, end: usize, filename_opt: Filename) -> String {
        let search_result = SearchResult {
            source: text.to_string(),
            start,
            end,
        };
        let config = Config {
            query: "".to_string(),
            case_sensitive: false,
            filename: match filename_opt {
                Filename::Include(name) => Some(name.to_string()),
                Filename::Omit => None,
            }
        };

        search_result.print(&config)
    }
    const RED: &str = "\x1b[31;1m";
    const END: &str = "\x1b[m";
    const TEAL: &str = "\x1b[36m";

    #[test]
    fn one_line_beginning() {
        assert_eq!(
            make("something", 0, 3, Filename::Omit),
            format!("{RED}som{END}ething")
        );
        assert_eq!(
            make("something", 0, 3, Filename::Include("foo.md")),
            format!("{TEAL}foo.md:{END}{RED}som{END}ething")
        );
    }

    #[test]
    fn one_line_middle() {
        assert_eq!(
            make("something", 1, 3, Filename::Omit),
            format!("s{RED}om{END}ething")
        );
        assert_eq!(
            make("something", 1, 3, Filename::Include("bar.md")),
            format!("{TEAL}bar.md:{END}s{RED}om{END}ething")
        );
    }

    #[test]
    fn one_line_end() {
        assert_eq!(
            make("something", 4, 9, Filename::Omit),
            format!("some{RED}thing{END}")
        );
        assert_eq!(
            make("something", 4, 9, Filename::Include("baz.md")),
            format!("{TEAL}baz.md:{END}some{RED}thing{END}")
        );
    }

    #[test]
    fn one_line_complete() {
        assert_eq!(
            make("something", 0, 9, Filename::Omit),
            format!("{RED}something{END}")
        );
        assert_eq!(
            make("something", 0, 9, Filename::Include("qux.md")),
            format!("{TEAL}qux.md:{END}{RED}something{END}")
        );
    }

    #[test]
    fn two_lines_beginning() {
        assert_eq!(
            make("some\nthing", 0, 6, Filename::Omit),
            format!("{RED}some{END}\n{RED}t{END}hing")
        );
        assert_eq!(
            make("some\nthing", 0, 6, Filename::Include("bar.md")),
            format!("{TEAL}bar.md:{END}{RED}some{END}\n{TEAL}bar.md:{END}{RED}t{END}hing")
        );
    }

    #[test]
    fn two_lines_middle() {
        assert_eq!(
            make("some\nthing", 3, 6, Filename::Omit),
            format!("som{RED}e{END}\n{RED}t{END}hing")
        );
        assert_eq!(
            make("some\nthing", 3, 6, Filename::Include("bar.md")),
            format!("{TEAL}bar.md:{END}som{RED}e{END}\n{TEAL}bar.md:{END}{RED}t{END}hing")
        );
    }

    #[test]
    fn two_lines_end() {
        assert_eq!(
            make("some\nthing", 3, 10, Filename::Omit),
            format!("som{RED}e{END}\n{RED}thing{END}")
        );
        assert_eq!(
            make("some\nthing", 3, 10, Filename::Include("bar.md")),
            format!("{TEAL}bar.md:{END}som{RED}e{END}\n{TEAL}bar.md:{END}{RED}thing{END}")
        );
    }

    #[test]
    fn two_lines_complete() {
        assert_eq!(
            make("some\nthing", 0, 10, Filename::Omit),
            format!("{RED}some{END}\n{RED}thing{END}")
        );
        assert_eq!(
            make("some\nthing", 0, 10, Filename::Include("bar.md")),
            format!("{TEAL}bar.md:{END}{RED}some{END}\n{TEAL}bar.md:{END}{RED}thing{END}")
        );
    }
}
