use std::collections::VecDeque;

pub struct InfinitePeekable<T: Iterator> {
    iter: T,
    buffer: VecDeque<T::Item>,
}

impl<T: Iterator> InfinitePeekable<T> {
    pub fn new(iter: T) -> Self {
        Self {
            iter: iter,
            buffer: VecDeque::new(),
        }
    }

    pub fn peek(&mut self, index: usize) -> Option<&T::Item> {
        // TODO: Find a cleaner way to express this (matching on the result of
        // `get` causes ownership problems).
        if index < self.buffer.len() {
            return self.buffer.get(index);
        }

        if let Some(next) = self.iter.next() {
            self.buffer.push_back(next);
            self.peek(index)
        } else {
            None
        }
    }
}

impl<T: Iterator> Iterator for InfinitePeekable<T> {
    type Item = T::Item;

    fn next(&mut self) -> Option<T::Item> {
        if let Some(front) = self.buffer.pop_front() {
            return Some(front);
        }

        self.iter.next()
    }
}

#[cfg(test)]
mod peek {
    use super::InfinitePeekable;

    #[test]
    fn not_consuming() {
        let mut x = InfinitePeekable::new("hey".chars());

        assert_eq!(x.peek(0), Some(&'h'));
        assert_eq!(x.peek(0), Some(&'h'));
        assert_eq!(x.peek(1), Some(&'e'));
        assert_eq!(x.peek(1), Some(&'e'));
        assert_eq!(x.peek(2), Some(&'y'));
        assert_eq!(x.peek(2), Some(&'y'));
        assert_eq!(x.peek(3), None);
        assert_eq!(x.peek(3), None);
        assert_eq!(x.peek(4), None);
        assert_eq!(x.peek(4), None);
    }

    #[test]
    fn consuming() {
        let mut x = InfinitePeekable::new("hey2".chars());

        assert_eq!(x.next(), Some('h'));
        assert_eq!(x.peek(0), Some(&'e'));
        assert_eq!(x.peek(0), Some(&'e'));
        assert_eq!(x.peek(1), Some(&'y'));
        assert_eq!(x.peek(1), Some(&'y'));
        assert_eq!(x.peek(2), Some(&'2'));
        assert_eq!(x.peek(2), Some(&'2'));
        assert_eq!(x.peek(3), None);
        assert_eq!(x.peek(3), None);
        assert_eq!(x.peek(4), None);
        assert_eq!(x.peek(4), None);
    }

    #[test]
    fn consumed() {
        let mut x = InfinitePeekable::new("hey".chars());

        assert_eq!(x.next(), Some('h'));
        assert_eq!(x.next(), Some('e'));
        assert_eq!(x.next(), Some('y'));
        assert_eq!(x.next(), None);
        assert_eq!(x.peek(0), None);
        assert_eq!(x.peek(0), None);
        assert_eq!(x.peek(1), None);
        assert_eq!(x.peek(1), None);
    }

    #[test]
    fn peek_then_consume() {
        let mut x = InfinitePeekable::new("hey".chars());

        assert_eq!(x.peek(0), Some(&'h'));
        assert_eq!(x.peek(0), Some(&'h'));
        assert_eq!(x.next(), Some('h'));
        assert_eq!(x.peek(0), Some(&'e'));
        assert_eq!(x.peek(0), Some(&'e'));
        assert_eq!(x.peek(1), Some(&'y'));
        assert_eq!(x.peek(1), Some(&'y'));
        assert_eq!(x.peek(2), None);
        assert_eq!(x.peek(2), None);
    }
}
