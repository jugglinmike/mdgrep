#[derive(PartialEq)]
enum State { Leading, Text(char, usize) }

// > A line consisting of optionally up to three spaces of indentation,
// > followed by a sequence of three or more matching `-`, `_`, or `*`
// > characters, each followed optionally by any number of spaces or tabs,
// > forms a thematic break.
//
// This too would be a heck of a lot easier with the regex crate.
pub fn is_thematic_break(content: &String) -> bool {
    let mut state = State::Leading;

    for (index, character) in content.chars().enumerate() {
        match state {
            State::Leading => {
                if character == ' ' && index < 3 {
                    continue;
                }

                if character == '-' || character == '_' || character == '*' {
                    state = State::Text(character, 1);
                } else {
                    return false;
                }
            }
            State::Text(previous, count) => {
                if character.is_ascii_whitespace() {
                    continue;
                } else if character == previous {
                    state = State::Text(character, count + 1);
                } else {
                    return false;
                }
            }
        }
    }

    match state {
        State::Leading => false,
        State::Text(_, count) => count >= 3,
    }
}

#[cfg(test)]
mod setext {
    fn run(text: &str) -> bool {
        super::is_thematic_break(&String::from(text))
    }

    #[test]
    fn empty() {
        assert_eq!(run(""), false);
        assert_eq!(run(" "), false);
        assert_eq!(run("\t"), false);
        assert_eq!(run(" \t"), false);
        assert_eq!(run("\t "), false);
    }

    #[test]
    fn markers() {
        assert_eq!(run("---"), true);
        assert_eq!(run("___"), true);
        assert_eq!(run("***"), true);
    }

    #[test]
    fn extra() {
        assert_eq!(run("----"), true);
        assert_eq!(run("-----"), true);
        assert_eq!(run("------"), true);
    }

    #[test]
    fn leading_space() {
        assert_eq!(run(" ---"), true);
        assert_eq!(run("  ---"), true);
        assert_eq!(run("   ---"), true);
    }

    #[test]
    fn trailing_space() {
        assert_eq!(run("- --"), true);
        assert_eq!(run("-\t--"), true);
        assert_eq!(run("-  --"), true);
        assert_eq!(run("- \t--"), true);
        assert_eq!(run("-\t --"), true);
    }

    #[test]
    fn invalid() {
        assert_eq!(run("--"), false);
        assert_eq!(run("_--"), false);
        assert_eq!(run("-_-"), false);
        assert_eq!(run("--_"), false);
        assert_eq!(run("    ---"), false);
        assert_eq!(run("\t---"), false);
        assert_eq!(run("---="), false);
        assert_eq!(run("--- a"), false);
    }
}
