pub mod config;
mod search_result;
pub mod block_tokens;
pub mod markdown_events;
pub mod read_utf8;
mod infinite_peekable;
mod setext;
mod stanza;
mod thematic_break;
mod search;

use stanza::Stanzas;
use search::Search;

pub fn run<T: std::io::Read>(stream: T, config: &config::Config) -> () {
    let events = markdown_events::MarkdownEvents::new(stream);
    let search = Search::new(Stanzas::new(events), config);

    for search_result in search {
        println!("{}", search_result.print(config));
    }
}
