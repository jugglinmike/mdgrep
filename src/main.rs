use std::env;
use std::process;
use std::fs;
use std::io::BufReader;

mod cli;

fn main() -> std::io::Result<()> {
    let args = env::args().collect::<Vec<_>>();

    let (filenames, mut config) = cli::parse_args(&args).unwrap_or_else(|error| {
        eprintln!("Problem parsing arguments: {}", error);
        process::exit(1);
    });
    let include_filenames = filenames.len() > 1;

    for filename in filenames {
        let bytes = BufReader::new(fs::File::open(&filename)?);
        if include_filenames {
            config.filename = Some(filename);
        }

        mdgrep::run(bytes, &config);
    }

    Ok(())
}
