use std::collections::VecDeque;
use super::block_tokens;
use block_tokens::Token as Token;
use block_tokens::TokenKind as TokenKind;
use block_tokens::{ListItemMarker, ListItemMarkerKind};
use block_tokens::CodeFence;
use crate::setext::setext_level;
use crate::thematic_break::is_thematic_break;
use std::fmt;

#[derive(Clone, PartialEq, Debug)]
pub enum Delta { Open, Close, Empty }

#[derive(Clone, Debug, PartialEq)]
pub enum ContainerBlock {
    BlockQuote,
    List(ListItemMarker),
    ListItem(ListItemMarker),
}

#[derive(Clone, Debug, PartialEq)]
pub enum CodeBlockKind {
    Indented,
    Fenced(CodeFence, usize),
}

#[derive(Clone, Debug, PartialEq)]
pub enum LeafBlock {
    Code(CodeBlockKind),
    Paragraph,
    Heading(usize),
    ThematicBreak,
}

fn supports_content(leaf: &LeafBlock) -> bool {
    return leaf != &LeafBlock::ThematicBreak;
}

#[derive(Clone, PartialEq)]
pub enum Event {
    ContainerBlock(ContainerBlock, Delta),
    LeafBlock(LeafBlock, Delta),
    Text(usize, String),
    SourceLine(usize, String),
}

impl fmt::Debug for Event {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Event::ContainerBlock(block, delta) => {
                let lead = match delta {
                    Delta::Open => "OPEN",
                    Delta::Close => "CLOSE",
                    Delta::Empty => "EMPTY",
                };
                let name = match block {
                    ContainerBlock::BlockQuote => "BlockQuote".to_string(),
                    ContainerBlock::List(marker) => format!("List(type={:?})", marker),
                    ContainerBlock::ListItem(marker) => format!("ListItem(type={:?})", marker)
                };
                write!(f, "{}:{}", lead, name)
            }
            Event::LeafBlock(leaf, delta) => {
                let lead = match delta {
                    Delta::Open => "OPEN",
                    Delta::Close => "CLOSE",
                    Delta::Empty => "EMPTY",
                };
                let name = match leaf {
                    LeafBlock::Code(kind) => format!("Code({:?})", kind),
                    LeafBlock::Paragraph => "Paragraph".to_string(),
                    LeafBlock::Heading(level) => format!("Heading(level={})", level),
                    LeafBlock::ThematicBreak => "Break".to_string(),
                };
                write!(f, "{}:{}", lead, name)
            }
            Event::Text(offset, string) => {
                write!(f, "TEXT@{}:\"{}\"", offset, string)
            }
            Event::SourceLine(offset, string) => {
                write!(f, "SRC@{}:\"{}\"", offset, string)
            }
        }
    }
}


#[derive(Debug)]
pub struct Line {
    closed_containers: usize,
    new_containers: Vec<ContainerBlock>,
    leaf: Option<LeafBlock>,
    text: Option<Token>,
}

impl Line {
    pub fn new() -> Line {
        Line {
            closed_containers: 0,
            new_containers: vec![],
            leaf: None,
            text: None,
        }
    }
}

pub struct Context {
    containers: Vec<ContainerBlock>,
    leaf: Option<LeafBlock>,
    events: VecDeque<Event>,
    text: VecDeque<(usize, String)>,
}

impl Context {
    pub fn new() -> Context {
        Context {
            containers: vec![],
            leaf: None,
            events: VecDeque::new(),
            text: VecDeque::new(),
        }
    }

    pub fn in_paragraph(&self) -> bool {
        self.leaf == Some(LeafBlock::Paragraph)
    }

    pub fn list_item_on_top(&self) -> bool {
        matches!(self.containers.last(), Some(ContainerBlock::ListItem(_)))
    }

    pub fn pop_leaf(&mut self) {
        let leaf = match &self.leaf {
            Some(leaf) => leaf,
            None => return,
        };

        if leaf == &LeafBlock::Paragraph {
            self.events.push_back(Event::LeafBlock(leaf.clone(), Delta::Open));
        }

        while let Some((position, mut content)) = self.text.pop_front() {
            // Only trim trailing space from the final text node in a block
            // because all other trailing space is eligible for a hard line
            // break.
            if self.text.is_empty() {
                content = content.trim_end().to_string();
            }

            self.events.push_back(Event::Text(position, content));
        }

        self.events.push_back(Event::LeafBlock(leaf.clone(), Delta::Close));
        self.leaf = None;
    }

    pub fn pop_container(&mut self) {
        if let Some(open) = &self.leaf {
            panic!("Cannot pop container while leaf block is open: {:?}", open);
        }
        if let Some(container) = self.containers.pop() {
            self.events.push_back(Event::ContainerBlock(container, Delta::Close));
        }
    }

    pub fn push_container(&mut self, container: ContainerBlock) {
        if let Some(open) = &self.leaf {
            panic!("Cannot push container while leaf block is open: {:?}", open);
        }

        self.events.push_back(Event::ContainerBlock(container.clone(), Delta::Open));
        self.containers.push(container);
    }

    pub fn push_leaf(&mut self, leaf: LeafBlock) {
        if let Some(open) = &self.leaf {
            panic!("Cannot push leaf while leaf block is open: {:?}", open);
        }

        if !supports_content(&leaf) {
            self.events.push_back(Event::LeafBlock(leaf.clone(), Delta::Empty));
        } else {
            self.leaf = Some(leaf.clone());

            if leaf != LeafBlock::Paragraph {
                self.events.push_back(Event::LeafBlock(leaf.clone(), Delta::Open));
            }
        }
    }

    pub fn push_text(&mut self, position: usize, content: String) {
        self.text.push_back((position, content));
    }

    pub fn to_heading(&mut self, level: usize) {
        let heading = LeafBlock::Heading(level);
        self.leaf = Some(heading.clone());
        self.events.push_back(Event::LeafBlock(heading, Delta::Open));
    }

    #[allow(dead_code)]
    pub fn width(&self) -> usize {
        let mut width = 0;

        for container in &self.containers {
            width += match container {
                ContainerBlock::BlockQuote => 2,
                ContainerBlock::List(_) => 0,
                ContainerBlock::ListItem(marker) => marker.width(),
            };
        }

        width
    }
}

pub struct MarkdownEvents<T: std::io::Read> {
    iter: block_tokens::Tokens<T>,
    context: Context,
}

impl<T: std::io::Read> MarkdownEvents<T> {
    pub fn new(source: T) -> MarkdownEvents<T> {
        MarkdownEvents {
            iter: block_tokens::Tokens::new(source),
            context: Context::new(),
        }
    }

    fn collect_line(&mut self, lazy_eligible: bool) -> Option<Line> {
        let (line_start, mut tokens) = match self.iter.line() {
            None => return None,
            Some(tokens) => tokens,
        };

        let line_source = tokens
            .iter()
            .take_while(|token| token.kind != TokenKind::Newline)
            .map(|token| token.source.clone())
            .collect::<Vec<_>>()
            .join("");

        self.context.events
            .push_back(Event::SourceLine(line_start, line_source));

        let mut line = Line::new();

        // Determine how many previously-opened container blocks need to be
        // closed.
        for (context_index, context_container) in self.context.containers.iter().enumerate() {
            let next_token = tokens.front();
            let space_count = block_tokens::count_spaces(&tokens);
            let mut pop = true;

            let mismatch = match context_container {
                ContainerBlock::BlockQuote => {
                    match next_token {
                        None => self.context.leaf != None,
                        Some(token) => {
                            match token.kind {
                                TokenKind::Text => {
                                    pop = false;
                                    lazy_eligible
                                },
                                TokenKind::BlockQuote => false,
                                TokenKind::Space => false,
                                _ => true,
                            }
                        }
                    }
                },
                ContainerBlock::ListItem(marker) => {
                    match next_token {
                        None => self.context.leaf == None,
                        Some(token) => {
                            match token.kind {
                                TokenKind::Space => {
                                    let width = marker.width();
                                    if space_count >= width {
                                        for _ in 1..width {
                                            tokens.pop_front();
                                        }
                                        false
                                    } else {
                                        true
                                    }
                                },
                                TokenKind::Text => {
                                    pop = false;
                                    false
                                },
                                _ => true
                            }
                        }
                    }
                },
                ContainerBlock::List(marker) => {
                    pop = false;

                    match next_token {
                        None => self.context.leaf == None,
                        Some(token) => {
                            match &token.kind {
                                TokenKind::Text => lazy_eligible,
                                TokenKind::Space => {
                                    // Use the marker from the previous list
                                    // item, if available, otherwise, use the
                                    // marker from the current list container.
                                    let relevant_marker = self.context
                                        .containers
                                        .get(context_index + 1)
                                        .and_then(|token| match &token {
                                            ContainerBlock::ListItem(item_marker) => {
                                                Some(item_marker)
                                            },
                                            _ => None,
                                        })
                                        .filter(|item_marker| marker.matches(item_marker))
                                        .unwrap_or(marker);

                                    space_count < relevant_marker.width()
                                },
                                TokenKind::ListItem(next_marker) => !marker.matches(next_marker),
                                _ => true,
                            }
                        }
                    }
                },
            };

            if mismatch {
                line.closed_containers = self.context.containers.len() - context_index;
                break;
            }
            if pop {
                tokens.pop_front();
            }
        }

        let front_token_kind = tokens.front().map(|token| &token.kind);

        // Integer-based list items can only interrupt paragraphs if they use
        // the number one, e.g. `1. ` or `1) `. If they use any other number,
        // they should instead be considered a continuation of the paragraph.
        if let Some(TokenKind::ListItem(marker)) = front_token_kind {
            if let ListItemMarkerKind::Integer(_, 2..) = marker.kind {
                if self.context.in_paragraph() && !self.context.list_item_on_top() {
                    line.text = block_tokens::to_text(&tokens, 0);
                    return Some(line);
                }
            }
        }

        if line.closed_containers == 0 {
            if let Some(LeafBlock::Code(CodeBlockKind::Fenced(opening_fence, indent))) = &self.context.leaf {
                let is_closing = match front_token_kind {
                    Some(TokenKind::CodeFence(fence)) => fence.closes(opening_fence),
                    _ => false,
                };
                if !is_closing {
                    line.text = block_tokens::to_text(&tokens, *indent);
                    return Some(line);
                }
            }
        }

        let mut space_count = 0;

        while let Some(next) = tokens.pop_front() {
            let prev_space_count = space_count;
            space_count = 0;

            line.new_containers.push(match &next.kind {
                TokenKind::BlockQuote => ContainerBlock::BlockQuote,
                TokenKind::ListItem(marker) => ContainerBlock::ListItem(marker.clone()),
                TokenKind::Space => {
                    if block_tokens::count_spaces(&tokens) < 3 {
                        space_count = prev_space_count + 1;
                        continue;
                    }
                    line.leaf = Some(LeafBlock::Code(CodeBlockKind::Indented));
                    tokens.pop_front();
                    tokens.pop_front();
                    tokens.pop_front();
                    line.text = block_tokens::to_text(&tokens, 0);
                    break;
                },
                TokenKind::Text => {
                    if line.leaf == None {
                        line.leaf = Some(LeafBlock::Paragraph);
                    }
                    line.text = Some(next);
                    continue;
                },
                TokenKind::Heading(level) => {
                    line.leaf = Some(LeafBlock::Heading(*level));
                    continue;
                },
                TokenKind::CodeFence(fence) => {
                    line.leaf = Some(LeafBlock::Code(
                        CodeBlockKind::Fenced(fence.clone(), prev_space_count)
                    ));
                    break;
                },
                TokenKind::Newline => {
                    break;
                },
            });
        }

        Some(line)
    }

    fn interpret_leaf(&mut self, leaf: Option<LeafBlock>, text: Option<Token>) {
        if let Some(text) = &text {
            if let (true, Some(level)) = (self.context.in_paragraph(), setext_level(&text.source)) {
                self.context.to_heading(level);
                self.context.pop_leaf(); // TODO write a test for this or remove it
                return;
            } else if is_thematic_break(&text.source) {
                self.context.pop_leaf();
                self.context.push_leaf(LeafBlock::ThematicBreak);
                return;
            }
        }

        if let (None, Some(new)) = (self.context.leaf.as_ref(), leaf.clone()) {
            self.context.push_leaf(new.clone());
        } else if let Some(LeafBlock::Heading(level)) = leaf {
            self.context.pop_leaf();

            self.context.push_leaf(LeafBlock::Heading(level));
        } else if let (Some(current), Some(_)) = (self.context.leaf.as_ref(), leaf) {
            if let LeafBlock::Code(CodeBlockKind::Fenced(_, _)) = current {
                self.context.pop_leaf();
            }
        }

        if let Some(text) = text {
            self.context.push_text(text.start, text.source);
        }

        if let Some(LeafBlock::Heading(_)) = self.context.leaf {
            self.context.pop_leaf();
        }
    }
}

impl<T: std::io::Read> Iterator for MarkdownEvents<T> {
    type Item = Event;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(next) = self.context.events.pop_front() {
            return Some(next);
        }
        let mut lazy_eligible = false;

        while let Some(line) = self.collect_line(lazy_eligible) {
            lazy_eligible = !matches!(line.leaf, Some(LeafBlock::Paragraph));

            if let Some(leaf) = &self.context.leaf {
                let is_opening = line.closed_containers > 0;
                let is_closing = !line.new_containers.is_empty();
                let close_leaf = match leaf {
                    LeafBlock::Paragraph => is_opening || is_closing || line.text.is_none(),
                    LeafBlock::Code(CodeBlockKind::Indented) => is_opening || is_closing,
                    _ => false,
                };

                if close_leaf {
                    self.context.pop_leaf();
                }
            }

            for _ in 0..line.closed_containers {
                self.context.pop_leaf();
                self.context.pop_container();
            }

            for block in line.new_containers {
                if let ContainerBlock::ListItem(marker) = &block {
                    if let Some(ContainerBlock::List(_)) = self.context.containers.last() {
                    } else {
                        self.context.push_container(
                            ContainerBlock::List(marker.clone())
                        );
                    }
                }

                self.context.push_container(block);
            }

            self.interpret_leaf(line.leaf, line.text);
        }

        self.context.pop_leaf();

        while !self.context.containers.is_empty() {
            self.context.pop_container();
        }

        self.context.events.pop_front()
    }
}

#[cfg(test)]
mod markdown_events {
    use super::*;
    use block_tokens::{CodeFenceMarker};
    use Delta::*;
    use block_tokens::ListItemMarkerSeparator::*;

    fn compare(source: &'static str, expected: Vec<Event>) {
        assert_eq!(
            MarkdownEvents::new(std::io::Cursor::new(source)).collect::<Vec<Event>>(),
            expected,
            "\nSource: {:?}",
            source
        );
    }

    fn t(index: usize, slice: &str) -> Event {
        Event::Text(index, slice.to_string())
    }

    fn source(index: usize, slice: &str) -> Event {
        Event::SourceLine(index, slice.to_string())
    }

    fn e_paragraph(delta: Delta) -> Event {
        Event::LeafBlock(LeafBlock::Paragraph, delta)
    }

    fn e_heading(level: usize, delta: Delta) -> Event {
        Event::LeafBlock(LeafBlock::Heading(level), delta)
    }

    fn e_fenced_code_block(indent: usize, width: usize, marker: CodeFenceMarker, info_str: Option<&str>, delta: Delta) -> Event {
        let info = info_str.map(|value| value.to_string());
        let fence = CodeFence { width, info, marker };
        let kind = CodeBlockKind::Fenced(fence, indent);
        Event::LeafBlock(LeafBlock::Code(kind), delta)
    }

    fn e_indented_code_block(delta: Delta) -> Event {
        Event::LeafBlock(LeafBlock::Code(CodeBlockKind::Indented), delta)
    }

    fn e_thematic_break() -> Event {
        Event::LeafBlock(LeafBlock::ThematicBreak, Empty)
    }

    fn e_block_quote(delta: Delta) -> Event {
        Event::ContainerBlock(ContainerBlock::BlockQuote, delta)
    }

    fn e_list(kind: ListItemMarkerKind, space: usize, delta: Delta) -> Event {
        let marker = ListItemMarker { kind, space };
        Event::ContainerBlock(ContainerBlock::List(marker), delta)
    }

    fn e_list_item(kind: ListItemMarkerKind, space: usize, delta: Delta) -> Event {
        let marker = ListItemMarker { kind, space };
        Event::ContainerBlock(ContainerBlock::ListItem(marker), delta)
    }

    #[test]
    fn paragraph_single_line() {
        compare(
            "a b c",
            vec![
                source(0, "a b c"),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn paragraph_two_lines_unix() {
        compare(
            "a b c\nd e f",
            vec![
                source(0, "a b c"),
                source(6, "d e f"),
                e_paragraph(Open),
                t(0, "a b c"),
                t(6, "d e f"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn paragraph_two_lines_windows() {
        compare(
            "a b c\r\nd e f",
            vec![
                source(0, "a b c"),
                source(7, "d e f"),
                e_paragraph(Open),
                t(0, "a b c"),
                t(7, "d e f"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn paragraph_interior_number() {
        compare(
            "a\n2. b",
            vec![
                source(0, "a"),
                source(2, "2. b"),
                e_paragraph(Open),
                t(0, "a"),
                t(2, "2. b"),
                e_paragraph(Close)
            ]
        );
        compare(
            "a\n2) b",
            vec![
                source(0, "a"),
                source(2, "2) b"),
                e_paragraph(Open),
                t(0, "a"),
                t(2, "2) b"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn two_paragraphs_unix() {
        compare(
            "a b c\n\nd e f",
            vec![
                source(0, "a b c"),
                source(6, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(7, "d e f"),
                e_paragraph(Open),
                t(7, "d e f"),
                e_paragraph(Close)
            ]
        );
        compare(
            "a b c\n\n\nd e f",
            vec![
                source(0, "a b c"),
                source(6, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(7, ""),
                source(8, "d e f"),
                e_paragraph(Open),
                t(8, "d e f"),
                e_paragraph(Close)
            ]
        );
        compare(
            "a b c\n\n\n\nd e f",
            vec![
                source(0, "a b c"),
                source(6, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(7, ""),
                source(8, ""),
                source(9, "d e f"),
                e_paragraph(Open),
                t(9, "d e f"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn two_paragraphs_windows() {
        compare(
            "a b c\r\n\r\nd e f",
            vec![
                source(0, "a b c"),
                source(7, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(9, "d e f"),
                e_paragraph(Open),
                t(9, "d e f"),
                e_paragraph(Close)
            ]
        );
        compare(
            "a b c\r\n\r\n\r\nd e f",
            vec![
                source(0, "a b c"),
                source(7, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(9, ""),
                source(11, "d e f"),
                e_paragraph(Open),
                t(11, "d e f"),
                e_paragraph(Close)
            ]
        );
        compare(
            "a b c\r\n\r\n\r\n\r\nd e f",
            vec![
                source(0, "a b c"),
                source(7, ""),
                e_paragraph(Open),
                t(0, "a b c"),
                e_paragraph(Close),
                source(9, ""),
                source(11, ""),
                source(13, "d e f"),
                e_paragraph(Open),
                t(13, "d e f"),
                e_paragraph(Close)
            ]
        );
    }

    #[test]
    fn headings() {
        compare(
            "# Hello\n## World!",
            vec![
                source(0, "# Hello"),
                e_heading(1, Open),
                t(2, "Hello"),
                e_heading(1, Close),
                source(8, "## World!"),
                e_heading(2, Open),
                t(11, "World!"),
                e_heading(2, Close)
            ]
        );
        compare(
            "### Things\nAnd stuff",
            vec![
                source(0, "### Things"),
                e_heading(3, Open),
                t(4, "Things"),
                e_heading(3, Close),
                source(11, "And stuff"),
                e_paragraph(Open),
                t(11, "And stuff"),
                e_paragraph(Close)
            ]
        );
        compare(
            "Things\n## And stuff",
            vec![
                source(0, "Things"),
                source(7, "## And stuff"),
                e_paragraph(Open),
                t(0, "Things"),
                e_paragraph(Close),
                e_heading(2, Open),
                t(10, "And stuff"),
                e_heading(2, Close),
            ]
        );
    }

    #[test]
    fn setext_headings() {
        compare(
            "Hello World!\n=",
            vec![
                source(0, "Hello World!"),
                source(13, "="),
                e_heading(1, Open),
                t(0, "Hello World!"),
                e_heading(1, Close)
            ]
        );
        compare(
            "Hello World!\n==",
            vec![
                source(0, "Hello World!"),
                source(13, "=="),
                e_heading(1, Open),
                t(0, "Hello World!"),
                e_heading(1, Close)
            ]
        );
        compare(
            "Hello\nWorld!\n==",
            vec![
                source(0, "Hello"),
                source(6, "World!"),
                source(13, "=="),
                e_heading(1, Open),
                t(0, "Hello"),
                t(6, "World!"),
                e_heading(1, Close)
            ]
        );
        compare(
            "Hello World!\n-",
            vec![
                source(0, "Hello World!"),
                source(13, "-"),
                e_heading(2, Open),
                t(0, "Hello World!"),
                e_heading(2, Close)
            ]
        );
        compare(
            "Hello World!\n--",
            vec![
                source(0, "Hello World!"),
                source(13, "--"),
                e_heading(2, Open),
                t(0, "Hello World!"),
                e_heading(2, Close)
            ]
        );
        compare(
            "Hello\nWorld!\n--",
            vec![
                source(0, "Hello"),
                source(6, "World!"),
                source(13, "--"),
                e_heading(2, Open),
                t(0, "Hello"),
                t(6, "World!"),
                e_heading(2, Close)
            ]
        );
    }

    #[test]
    fn blockquote_single_line() {
        compare(
            "> a b c",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                e_paragraph(Open),
                t(2, "a b c"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_two_lines_unix() {
        compare(
            "> a b c\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(8, "> d e f"),
                e_paragraph(Open),
                t(2, "a b c"),
                t(10, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_two_lines_windows() {
        compare(
            "> a b c\r\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(9, "> d e f"),
                e_paragraph(Open),
                t(2, "a b c"),
                t(11, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_two_lines_lazy_unix() {
        compare(
            "> a b c\nd e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(8, "d e f"),
                e_paragraph(Open),
                t(2, "a b c"),
                t(8, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
        compare(
            "> a b c\n d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(8, " d e f"),
                e_paragraph(Open),
                t(2, "a b c"),
                t(9, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_two_lines_lazy_windows() {
        compare(
            "> a b c\r\nd e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(9, "d e f"),
                e_paragraph(Open),
                t(2, "a b c"),
                t(9, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_no_lazy() {
        // Lazy continuations only occur within paragraphs
        compare(
            "> # a\nb",
            vec![
                source(0, "> # a"),
                e_block_quote(Open),
                e_heading(1, Open),
                t(4, "a"),
                e_heading(1, Close),
                source(6, "b"),
                e_block_quote(Close),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
            ]
        );
    }

    #[test]
    fn blockquote_two_paragraphs_unix() {
        compare(
            "> a b c\n>\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(8, ">"),
                e_paragraph(Open),
                t(2, "a b c"),
                e_paragraph(Close),
                source(10, "> d e f"),
                e_paragraph(Open),
                t(12, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_two_paragrpahs_windows() {
        compare(
            "> a b c\r\n>\r\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(9, ">"),
                e_paragraph(Open),
                t(2, "a b c"),
                e_paragraph(Close),
                source(12, "> d e f"),
                e_paragraph(Open),
                t(14, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn two_blockquotes_unix() {
        compare(
            "> a b c\n\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(8, ""),
                e_paragraph(Open),
                t(2, "a b c"),
                e_paragraph(Close),
                e_block_quote(Close),
                source(9, "> d e f"),
                e_block_quote(Open),
                e_paragraph(Open),
                t(11, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn two_blockquotes_windows() {
        compare(
            "> a b c\r\n\r\n> d e f",
            vec![
                source(0, "> a b c"),
                e_block_quote(Open),
                source(9, ""),
                e_paragraph(Open),
                t(2, "a b c"),
                e_paragraph(Close),
                e_block_quote(Close),
                source(11, "> d e f"),
                e_block_quote(Open),
                e_paragraph(Open),
                t(13, "d e f"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn blockquote_nested_unix() {
        compare(
            "> a\n> > b\n> > c\n>\n> d",
            vec![
                source(0, "> a"),
                e_block_quote(Open),
                source(4, "> > b"),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                e_block_quote(Open),
                source(10, "> > c"),
                source(16, ">"),
                e_paragraph(Open),
                t(8, "b"),
                t(14, "c"),
                e_paragraph(Close),
                e_block_quote(Close),
                source(18, "> d"),
                e_paragraph(Open),
                t(20, "d"),
                e_paragraph(Close),
                e_block_quote(Close)
            ]
        );
    }

    #[test]
    fn list_items_single_line() {
        compare(
            "- a\n- b",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "- b"),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
        compare(
            "- a\n- b\n- c",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "- b"),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(8, "- c"),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_paragraph(Open),
                t(10, "c"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
        compare(
            "1. a\n2. b",
            vec![
                source(0, "1. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                source(5, "2. b"),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 2), 1, Open),
                e_paragraph(Open),
                t(8, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 2), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
            ]
        );
        compare(
            "1. a\n2345. b",
            vec![
                source(0, "1. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                source(5, "2345. b"),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 2345), 1, Open),
                e_paragraph(Open),
                t(11, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 2345), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
            ]
        );
    }

    #[test]
    fn list_items_mismatched() {
        compare(
            "- a\n* b",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "* b"),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Star, 1, Open),
                e_list_item(ListItemMarkerKind::Star, 1, Open),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Star, 1, Close),
                e_list(ListItemMarkerKind::Star, 1, Close),
            ]
        );
        compare(
            "1. a\n2) b",
            vec![
                source(0, "1. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                source(5, "2) b"),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list(ListItemMarkerKind::Integer(Paren, 2), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Paren, 2), 1, Open),
                e_paragraph(Open),
                t(8, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Paren, 2), 1, Close),
                e_list(ListItemMarkerKind::Integer(Paren, 2), 1, Close),
            ]
        );
    }

    #[test]
    fn list_items_two_lines() {
        compare(
            "- a\nb",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "b"),
                e_paragraph(Open),
                t(2, "a"),
                t(4, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
        compare(
            "- a\nb\n- c",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "b"),
                source(6, "- c"),
                e_paragraph(Open),
                t(2, "a"),
                t(4, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_paragraph(Open),
                t(8, "c"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
    }

    #[test]
    fn list_item_loose() {
        compare(
            "- a\n\n  b",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, ""),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                source(5, "  b"),
                e_paragraph(Open),
                t(7, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
        compare(
            "123456. a\n\n        b",
            vec![
                source(0, "123456. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                source(10, ""),
                e_paragraph(Open),
                t(8, "a"),
                e_paragraph(Close),
                source(11, "        b"),
                e_paragraph(Open),
                t(19, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
            ]
        );
        compare(
            "1. a\n23. b\n\n   c",
            vec![
                source(0, "1. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                source(5, "23. b"),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 23), 1, Open),
                source(11, ""),
                e_paragraph(Open),
                t(9, "b"),
                e_paragraph(Close),
                source(12, "   c"),
                e_list_item(ListItemMarkerKind::Integer(Dot, 23), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_paragraph(Open),
                t(15, "c"),
                e_paragraph(Close),
            ]
        );
        compare(
            "-  a\n\n   b",
            vec![
                source(0, "-  a"),
                e_list(ListItemMarkerKind::Dash, 2, Open),
                e_list_item(ListItemMarkerKind::Dash, 2, Open),
                source(5, ""),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                source(6, "   b"),
                e_paragraph(Open),
                t(9, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 2, Close),
                e_list(ListItemMarkerKind::Dash, 2, Close),
            ]
        );
    }

    #[test]
    fn list_item_tight() {
        compare(
            "123456. a\n\nb",
            vec![
                source(0, "123456. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                source(10, ""),
                e_paragraph(Open),
                t(8, "a"),
                e_paragraph(Close),
                source(11, "b"),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_paragraph(Open),
                t(11, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "123456. a\n\n b",
            vec![
                source(0, "123456. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                source(10, ""),
                e_paragraph(Open),
                t(8, "a"),
                e_paragraph(Close),
                source(11, " b"),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_paragraph(Open),
                t(12, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "123456. a\n\n  b",
            vec![
                source(0, "123456. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                source(10, ""),
                e_paragraph(Open),
                t(8, "a"),
                e_paragraph(Close),
                source(11, "  b"),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_paragraph(Open),
                t(13, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "123456. a\n\n       b",
            vec![
                source(0, "123456. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Open),
                source(10, ""),
                e_paragraph(Open),
                t(8, "a"),
                e_paragraph(Close),
                source(11, "       b"),
                e_list_item(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 123456), 1, Close),
                e_indented_code_block(Open),
                t(15, "   b"),
                e_indented_code_block(Close),
            ]
        );
        compare(
            "1. a\n23. b\n\n    c",
            vec![
                source(0, "1. a"),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                source(5, "23. b"),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 23), 1, Open),
                source(11, ""),
                e_paragraph(Open),
                t(9, "b"),
                e_paragraph(Close),
                source(12, "    c"),
                e_paragraph(Open),
                t(16, "c"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 23), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
            ]
        );
    }

    #[test]
    fn list_item_then_paragraph() {
        compare(
            "- a\n\nb",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, ""),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                source(5, "b"),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(5, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "- a\n\n b",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, ""),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                source(5, " b"),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "-  a\n\n b",
            vec![
                source(0, "-  a"),
                e_list(ListItemMarkerKind::Dash, 2, Open),
                e_list_item(ListItemMarkerKind::Dash, 2, Open),
                source(5, ""),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                source(6, " b"),
                e_list_item(ListItemMarkerKind::Dash, 2, Close),
                e_list(ListItemMarkerKind::Dash, 2, Close),
                e_paragraph(Open),
                t(7, "b"),
                e_paragraph(Close),
            ]
        );
        compare(
            "-  a\n\n  b",
            vec![
                source(0, "-  a"),
                e_list(ListItemMarkerKind::Dash, 2, Open),
                e_list_item(ListItemMarkerKind::Dash, 2, Open),
                source(5, ""),
                e_paragraph(Open),
                t(3, "a"),
                e_paragraph(Close),
                source(6, "  b"),
                e_list_item(ListItemMarkerKind::Dash, 2, Close),
                e_list(ListItemMarkerKind::Dash, 2, Close),
                e_paragraph(Open),
                t(8, "b"),
                e_paragraph(Close),
            ]
        );
        // Determines eligibility for lazy continuation based on the
        // indentation of the most recent list item, not the first list item.
        compare(
            "- a\n-  b\n\n  c",
            vec![
                source(0, "- a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                source(4, "-  b"),
                e_paragraph(Open),
                t(2, "a"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list_item(ListItemMarkerKind::Dash, 2, Open),
                source(9, ""),
                e_paragraph(Open),
                t(7, "b"),
                e_paragraph(Close),
                source(10, "  c"),
                e_list_item(ListItemMarkerKind::Dash, 2, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(12, "c"),
                e_paragraph(Close),
            ]
        );
        // Lazy continuations only occur within paragraphs
        compare(
            "- # a\nb",
            vec![
                source(0, "- # a"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_heading(1, Open),
                t(4, "a"),
                e_heading(1, Close),
                source(6, "b"),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(6, "b"),
                e_paragraph(Close),
            ]
        );
    }

    #[test]
    fn list_item_interrupts_paragraph() {
        compare(
            "a\n- b",
            vec![
                source(0, "a"),
                source(2, "- b"),
                e_paragraph(Open),
                t(0, "a"),
                e_paragraph(Close),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_paragraph(Open),
                t(4, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
        compare(
            "a\n* b",
            vec![
                source(0, "a"),
                source(2, "* b"),
                e_paragraph(Open),
                t(0, "a"),
                e_paragraph(Close),
                e_list(ListItemMarkerKind::Star, 1, Open),
                e_list_item(ListItemMarkerKind::Star, 1, Open),
                e_paragraph(Open),
                t(4, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Star, 1, Close),
                e_list(ListItemMarkerKind::Star, 1, Close),
            ]
        );
        compare(
            "a\n1. b",
            vec![
                source(0, "a"),
                source(2, "1. b"),
                e_paragraph(Open),
                t(0, "a"),
                e_paragraph(Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Open),
                e_paragraph(Open),
                t(5, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
                e_list(ListItemMarkerKind::Integer(Dot, 1), 1, Close),
            ]
        );
        compare(
            "a\n1) b",
            vec![
                source(0, "a"),
                source(2, "1) b"),
                e_paragraph(Open),
                t(0, "a"),
                e_paragraph(Close),
                e_list(ListItemMarkerKind::Integer(Paren, 1), 1, Open),
                e_list_item(ListItemMarkerKind::Integer(Paren, 1), 1, Open),
                e_paragraph(Open),
                t(5, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Integer(Paren, 1), 1, Close),
                e_list(ListItemMarkerKind::Integer(Paren, 1), 1, Close),
            ]
        );
    }

    #[test]
    fn code_block() {
        compare(
            "    a",
            vec![
                source(0, "    a"),
                e_indented_code_block(Open),
                t(4, "a"),
                e_indented_code_block(Close),
            ]
        );
        compare(
            "     a",
            vec![
                source(0, "     a"),
                e_indented_code_block(Open),
                t(4, " a"),
                e_indented_code_block(Close),
            ]
        );
        compare(
            "    > a",
            vec![
                source(0, "    > a"),
                e_indented_code_block(Open),
                t(4, "> a"),
                e_indented_code_block(Close),
            ]
        );
        compare(
            "    a\n    b",
            vec![
                source(0, "    a"),
                e_indented_code_block(Open),
                source(6, "    b"),
                t(4, "a"),
                t(10, "b"),
                e_indented_code_block(Close),
            ]
        );
        compare(
            "    a\n\n    b",
            vec![
                source(0, "    a"),
                e_indented_code_block(Open),
                source(6, ""),
                source(7, "    b"),
                t(4, "a"),
                t(11, "b"),
                e_indented_code_block(Close),
            ]
        );
    }

    #[test]
    fn code_block_then_list() {
        compare(
            "    a\n- b",
            vec![
                source(0, "    a"),
                e_indented_code_block(Open),
                source(6, "- b"),
                t(4, "a"),
                e_indented_code_block(Close),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_paragraph(Open),
                t(8, "b"),
                e_paragraph(Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
            ]
        );
    }

    #[test]
    fn thematic_break() {
        compare(
            "---",
            vec![source(0, "---"), e_thematic_break()]
        );
        compare(
            "paragraph 1\n***\nparagraph 2",
            vec![
                source(0, "paragraph 1"),
                source(12, "***"),
                e_paragraph(Open),
                t(0, "paragraph 1"),
                e_paragraph(Close),
                e_thematic_break(),
                source(16, "paragraph 2"),
                e_paragraph(Open),
                t(16, "paragraph 2"),
                e_paragraph(Close),
            ]
        );
        compare(
            "paragraph\n\n---",
            vec![
                source(0, "paragraph"),
                source(10, ""),
                e_paragraph(Open),
                t(0, "paragraph"),
                e_paragraph(Close),
                source(11, "---"),
                e_thematic_break(),
            ]
        );
        compare(
            "# heading\n---",
            vec![
                source(0, "# heading"),
                e_heading(1, Open),
                t(2, "heading"),
                e_heading(1, Close),
                source(10, "---"),
                e_thematic_break(),
            ]
        );
    }

    #[test]
    fn fenced_code() {
        compare(
            "```\n```",
            vec![
                source(0, "```"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(4, "```"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            "~~~\n```\n~~~",
            vec![
                source(0, "~~~"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Tilde, None, Open),
                source(4, "```"),
                source(8, "~~~"),
                t(4, "```"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Tilde, None, Close),
            ]
        );
        compare(
            "before\n```\ninside\n```\nafter",
            vec![
                source(0, "before"),
                source(7, "```"),
                e_paragraph(Open),
                t(0, "before"),
                e_paragraph(Close),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(11, "inside"),
                source(18, "```"),
                t(11, "inside"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                source(22, "after"),
                e_paragraph(Open),
                t(22, "after"),
                e_paragraph(Close),
            ]
        );
        compare(
            "```\na\n\nb\n```",
            vec![
                source(0, "```"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(4, "a"),
                source(6, ""),
                source(7, "b"),
                source(9, "```"),
                t(4, "a"),
                t(7, "b"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
    }

    #[test]
    fn fenced_code_width() {
        compare(
            "````\ninside 1\n```\ninside 2\n````\noutside",
            vec![
                source(0, "````"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Open),
                source(5, "inside 1"),
                source(14, "```"),
                source(18, "inside 2"),
                source(27, "````"),
                t(5, "inside 1"),
                t(14, "```"),
                t(18, "inside 2"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Close),
                source(32, "outside"),
                e_paragraph(Open),
                t(32, "outside"),
                e_paragraph(Close),
            ]
        );
        compare(
            "````\ninside\n````\noutside",
            vec![
                source(0, "````"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Open),
                source(5, "inside"),
                source(12, "````"),
                t(5, "inside"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Close),
                source(17, "outside"),
                e_paragraph(Open),
                t(17, "outside"),
                e_paragraph(Close),
            ]
        );
        compare(
            "````\ninside\n`````\noutside",
            vec![
                source(0, "````"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Open),
                source(5, "inside"),
                source(12, "`````"),
                t(5, "inside"),
                e_fenced_code_block(0, 4, CodeFenceMarker::Backtick, None, Close),
                source(18, "outside"),
                e_paragraph(Open),
                t(18, "outside"),
                e_paragraph(Close),
            ]
        );
    }

    #[test]
    fn fenced_code_nested() {
        compare(
            "```\n> - inside\n```",
            vec![
                source(0, "```"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(4, "> - inside"),
                source(15, "```"),
                t(4, "> - inside"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            "- ```\n   inside\n  ```\nafter",
            vec![
                source(0, "- ```"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "   inside"),
                source(16, "  ```"),
                t(8, " inside"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                source(22, "after"),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(22, "after"),
                e_paragraph(Close),
            ]
        );
    }

    #[test]
    fn fenced_code_indented() {
        compare(
            " ```\nx\n```",
            vec![
                source(0, " ```"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None, Open),
                source(5, "x"),
                source(7, "```"),
                t(5, "x"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            " ```\n x\n```",
            vec![
                source(0, " ```"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None, Open),
                source(5, " x"),
                source(8, "```"),
                t(6, "x"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            " ```\n  x\n```",
            vec![
                source(0, " ```"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None, Open),
                source(5, "  x"),
                source(9, "```"),
                t(6, " x"),
                e_fenced_code_block(1, 3, CodeFenceMarker::Backtick, None,Close),
            ]
        );
        compare(
            "  ```\nx\n```",
            vec![
                source(0, "  ```"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "x"),
                source(8, "```"),
                t(6, "x"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            "  ```\n x\n```",
            vec![
                source(0, "  ```"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, " x"),
                source(9, "```"),
                t(7, "x"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            "  ```\n  x\n```",
            vec![
                source(0, "  ```"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "  x"),
                source(10, "```"),
                t(8, "x"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
        compare(
            "  ```\n   x\n```",
            vec![
                source(0, "  ```"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "   x"),
                source(11, "```"),
                t(8, " x"),
                e_fenced_code_block(2, 3, CodeFenceMarker::Backtick, None, Close),
            ]
        );
    }

    #[test]
    fn fenced_code_interrupted() {
        compare(
            "- ```\nx",
            vec![
                source(0, "- ```"),
                e_list(ListItemMarkerKind::Dash, 1, Open),
                e_list_item(ListItemMarkerKind::Dash, 1, Open),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                e_list_item(ListItemMarkerKind::Dash, 1, Close),
                e_list(ListItemMarkerKind::Dash, 1, Close),
                e_paragraph(Open),
                t(6, "x"),
                e_paragraph(Close),
            ]
        );
        compare(
            "> ```\nx",
            vec![
                source(0, "> ```"),
                e_block_quote(Open),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(6, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                e_block_quote(Close),
                e_paragraph(Open),
                t(6, "x"),
                e_paragraph(Close),
            ]
        );
        compare(
            "> > ```\nx",
            vec![
                source(0, "> > ```"),
                e_block_quote(Open),
                e_block_quote(Open),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(8, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                e_block_quote(Close),
                e_block_quote(Close),
                e_paragraph(Open),
                t(8, "x"),
                e_paragraph(Close),
            ]
        );
        compare(
            "> > ```\n> x",
            vec![
                source(0, "> > ```"),
                e_block_quote(Open),
                e_block_quote(Open),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Open),
                source(8, "> x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, None, Close),
                e_block_quote(Close),
                e_paragraph(Open),
                t(10, "x"),
                e_paragraph(Close),
                e_block_quote(Close),
            ]
        );
    }

    #[test]
    fn fenced_code_info() {
        compare(
            "```foo\nx\n```",
            vec![
                source(0, "```foo"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("foo"), Open),
                source(7, "x"),
                source(9, "```"),
                t(7, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("foo"), Close),
            ]
        );
        compare(
            "```~\nx\n```",
            vec![
                source(0, "```~"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("~"), Open),
                source(5, "x"),
                source(7, "```"),
                t(5, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("~"), Close),
            ]
        );
        compare(
            "~~~`\nx\n~~~",
            vec![
                source(0, "~~~`"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Tilde, Some("`"), Open),
                source(5, "x"),
                source(7, "~~~"),
                t(5, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Tilde, Some("`"), Close),
            ]
        );
        compare(
            "``` trimmed \ttext\t\nx\n```",
            vec![
                source(0, "``` trimmed \ttext\t"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("trimmed \ttext"), Open),
                source(19, "x"),
                source(21, "```"),
                t(19, "x"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("trimmed \ttext"), Close),
            ]
        );
        compare(
            "```foo\n```foo\n```",
            vec![
                source(0, "```foo"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("foo"), Open),
                source(7, "```foo"),
                source(14, "```"),
                t(7, "```foo"),
                e_fenced_code_block(0, 3, CodeFenceMarker::Backtick, Some("foo"), Close),
            ]
        );
    }
}
