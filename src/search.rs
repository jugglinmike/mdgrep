use std::collections::VecDeque;
use super::config;
use super::stanza::{SourceLine, Stanza, Stanzas, Stuff};
use super::search_result::SearchResult;

pub struct Search<'a, T: std::io::Read> {
    stanzas: Stanzas<T>,
    config: &'a config::Config,
    source_lines: VecDeque<SourceLine>,
    search_results: VecDeque<SearchResult>,
}

impl<'a, T: std::io::Read> Search<'a, T> {
    pub fn new(stanzas: Stanzas<T>, config: &'a config::Config) -> Self {
        Self {
            stanzas,
            config,
            source_lines: VecDeque::new(),
            search_results: VecDeque::new(),
        }
    }

    fn search(&mut self, stanza: Stanza) -> VecDeque<SearchResult> {
        stanza.search(&self.config.query)
            .iter()
            .map(|range| {
                let start = range.start;
                let end = range.end;
                let relevant_lines = self.source_lines
                    .iter()
                    .filter(|source_line| {
                        let source_line_end = source_line.location + source_line.source.len();
                        if start >= source_line.location && start < source_line_end {
                            return true;
                        }
                        if end >= source_line.location && end <= source_line_end {
                            return true;
                        }
                        return false;
                    })
                    .collect::<Vec<_>>();
                let x = relevant_lines.first().unwrap().location;

                SearchResult::new(
                    relevant_lines
                        .iter()
                        .map(|source_line| source_line.source.clone())
                        .collect::<Vec<_>>()
                        .join("\n"),
                    start - x,
                    end - x
                )
            })
            .collect::<VecDeque<_>>()
    }
}

impl<'a, T: std::io::Read> Iterator for Search<'a, T> {
    type Item = SearchResult;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(result) = self.search_results.pop_front() {
            return Some(result);
        }

        match self.stanzas.next() {
            Some(Stuff::Stanza(stanza)) => {
                let mut results = self.search(stanza);
                self.search_results.append(&mut results);
            },
            Some(Stuff::SourceLine(source_line)) => {
                self.source_lines.push_back(source_line);
            },
            None => {
                if self.search_results.len() == 0 {
                    return None;
                }
            },
        }

        return self.next();
    }
}

#[cfg(test)]
mod search {
    use super::config::Config;
    use super::Search;
    use super::Stanzas;
    use super::SearchResult;
    use super::super::markdown_events::MarkdownEvents;

    fn search(needle: &str, haystack: &'static str) -> Vec<SearchResult> {
        let config = Config { query: needle.to_string(), case_sensitive: false, filename: None };
        let events = MarkdownEvents::new(std::io::Cursor::new(haystack));
        let stanzas = Stanzas::new(events);
        let search = Search::new(stanzas, &config);

        search.collect()
    }

    fn results(info: Vec<(&str, usize, usize)>) -> Vec<SearchResult> {
        info.iter()
            .map(|item| SearchResult::new(item.0.to_string(), item.1, item.2))
            .collect()
    }

    #[test]
    fn single_line_entirety() {
        assert_eq!(
            search("foo", "foo"),
            results(vec![("foo", 0, 3)])
        );
    }

    #[test]
    fn multiple_line_interior() {
        assert_eq!(
            search("4 5", "1 2\n3 4\n5 6"),
            results(vec![("3 4\n5 6", 2, 5)])
        );
    }

    #[test]
    fn multiple_line_beginning() {
        assert_eq!(
            search("3 4 5", "1 2\n3 4\n5 6"),
            results(vec![("3 4\n5 6", 0, 5)])
        );
    }

    #[test]
    fn multiple_line_end() {
        assert_eq!(
            search("4 5 6", "1 2\n3 4\n5 6"),
            results(vec![("3 4\n5 6", 2, 7)])
        );
    }
}
