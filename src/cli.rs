use mdgrep::config::Config;

pub fn parse_args<T>(args: &[T]) -> Result<(Vec<String>, Config), &'static str>
where T: AsRef<str>
{
    if args.len() < 1 {
        return Err("Not enough arguments")
    }
    let mut query: Option<String> = None;
    let mut filenames = Vec::new();
    let mut case_sensitive = true;

    for arg in args.iter().skip(1) {
        if arg.as_ref() == "-i" || arg.as_ref() == "--ignore-case" {
            case_sensitive = false;
        } else if query.is_none() {
            query = Some(arg.as_ref().to_string());
        } else {
            filenames.push(arg.as_ref().to_string());
        }
    }

    let config = Config {
        query: query.ok_or("A query is required")?,
        case_sensitive,
        filename: None,
    };
    if filenames.len() == 0 {
        Err("At least one file name is required")
    } else {
        Ok((filenames, config))
    }
}

#[cfg(test)]
mod parse_args {
    use super::*;

    #[test]
    fn nothing() {
        assert_eq!(
            parse_args(&[] as &[&str]).unwrap_err(),
            "Not enough arguments"
        );
    }

    #[test]
    fn no_query() {
        assert_eq!(
            parse_args(&["mdgrep"]).unwrap_err(),
            "A query is required"
        );
    }

    #[test]
    fn no_filename() {
        assert_eq!(
            parse_args(&["mdgrep", "x"]).unwrap_err(),
            "At least one file name is required"
        );
    }

    #[test]
    fn basic() {
        assert_eq!(
            parse_args(&["mdgrep", "x", "diary.md"]).unwrap(),
            (
                vec!["diary.md".to_owned()],
                Config { query: "x".to_owned(), case_sensitive: true, filename: None }
            ),
        );
    }

    #[test]
    fn ignore_case_short() {
        assert_eq!(
            parse_args(&["mdgrep", "x", "-i", "diary.md"]).unwrap(),
            (
                vec!["diary.md".to_owned()],
                Config { query: "x".to_owned(), case_sensitive: false, filename: None }
            ),
        );
    }

    #[test]
    fn ignore_case_long() {
        assert_eq!(
            parse_args(&["mdgrep", "x", "--ignore-case", "diary.md"]).unwrap(),
            (
                vec!["diary.md".to_owned()],
                Config { query: "x".to_owned(), case_sensitive: false, filename: None }
            ),
        );
    }
}
