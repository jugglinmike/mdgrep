use std::io::{BufRead, BufReader};

pub struct ContextReader<T: std::io::Read> {
    reader: std::io::Lines<BufReader<T>>,
    before: usize,
    after: usize,
    context: Vec<String>,
    context_start: usize,
    index: usize,
}

impl<T: std::io::Read> ContextReader<T> {
    pub fn new(source: T, before: usize, after: usize) -> Self {
        Self {
            reader: BufReader::new(source).lines(),
            before,
            after,
            context: vec![],
            context_start: 0,
            index: 0,
        }
    }

    pub fn get(&self, start: usize, end: usize) -> Vec<String> {
        self.context.iter()
            .enumerate()
            .filter(|x| {
                (start < self.before || x.0 >= start - self.before) && x.0 <= end + self.after
            })
            .map(|x| x.1.clone())
            .collect()
    }

    // TODO: The context currently grows indefinitely. Implement a method to
    // discard lines as they become irrelevant.
}

impl<T: std::io::Read> Iterator for ContextReader<T> {
    type Item = std::io::Result<String>;

    fn next(&mut self) -> Option<Self::Item> {
        while self.context_start + self.index + self.after >= self.context_start + self.context.len() {
            let next = self.reader.next();
            match next {
                Some(Ok(ref string)) => {
                    self.context.push(string.clone());
                },
                Some(Err(_)) => return next,
                None => break,
            };
        }

        let x = self.context.get(self.index).map(|x| Ok(x.clone()));
        self.index += 1;
        x
    }
}

#[cfg(test)]
mod context_reader {
    use super::ContextReader;

    fn make(string: &str, before: usize, after: usize) -> ContextReader<std::io::Cursor<&str>> {
        ContextReader::new(std::io::Cursor::new(string), before, after)
    }

    #[test]
    fn empty_lines() {
        let mut reader = make("I need empty lines\n\nTo distinguish paragraphs\r\n\r\nYes, even from Windows", 0, 0);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("I need empty lines"));
        assert_eq!(reader.next().unwrap().unwrap(), String::from(""));
        assert_eq!(reader.next().unwrap().unwrap(), String::from("To distinguish paragraphs"));
        assert_eq!(reader.next().unwrap().unwrap(), String::from(""));
        assert_eq!(reader.next().unwrap().unwrap(), String::from("Yes, even from Windows"));
    }

    #[test]
    fn one_line() {
        let mut reader = make("foo", 0, 0);
        assert_eq!(reader.next().unwrap().unwrap(), String::from("foo"));
        assert!(reader.next().is_none());

        assert_eq!(reader.get(0, 0), ["foo"]);
    }

    #[test]
    fn three_lines_no_context() {
        let mut reader = make("foo\nbar\r\nbaz", 0, 0);
        assert_eq!(reader.next().unwrap().unwrap(), String::from("foo"));
        assert_eq!(reader.get(0, 0), ["foo"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("bar"));
        assert_eq!(reader.get(1, 1), ["bar"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("baz"));
        assert_eq!(reader.get(2, 2), ["baz"]);

        assert!(reader.next().is_none());
    }

    #[test]
    fn three_lines_before_context() {
        let mut reader = make("foo\nbar\r\nbaz", 2, 0);
        assert_eq!(reader.next().unwrap().unwrap(), String::from("foo"));
        assert_eq!(reader.get(0, 0), ["foo"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("bar"));
        assert_eq!(reader.get(1, 1), ["foo", "bar"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("baz"));
        assert_eq!(reader.get(2, 2), ["foo", "bar", "baz"]);

        assert!(reader.next().is_none());
    }

    #[test]
    fn three_lines_after_context() {
        let mut reader = make("foo\nbar\r\nbaz", 0, 2);
        assert_eq!(reader.next().unwrap().unwrap(), String::from("foo"));
        assert_eq!(reader.get(0, 0), ["foo", "bar", "baz"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("bar"));
        assert_eq!(reader.get(1, 1), ["bar", "baz"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("baz"));
        assert_eq!(reader.get(2, 2), ["baz"]);

        assert!(reader.next().is_none());
    }

    #[test]
    fn three_lines_both_context() {
        let mut reader = make("foo\nbar\r\nbaz", 1, 1);
        assert_eq!(reader.next().unwrap().unwrap(), String::from("foo"));
        assert_eq!(reader.get(0, 0), ["foo", "bar"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("bar"));
        assert_eq!(reader.get(1, 1), ["foo", "bar", "baz"]);

        assert_eq!(reader.next().unwrap().unwrap(), String::from("baz"));
        assert_eq!(reader.get(2, 2), ["bar", "baz"]);

        assert!(reader.next().is_none());
    }
}
