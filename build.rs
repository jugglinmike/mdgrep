use std::{env, fs, path};
use std::collections::HashSet;
use std::io::Write;
use json;

type AnyError<T> = Result<T, Box<dyn std::error::Error>>;

fn main() -> AnyError<()> {
    let out_dir = env::var("OUT_DIR")?;
    let cargo_manifest_dir = env::var("CARGO_MANIFEST_DIR")?;

    let build_script_path = path_from(&cargo_manifest_dir, "build.rs");
    let tests_path = path_from(&cargo_manifest_dir, "tests/commonmark-0.30-tests.json");
    let expected_failures_path = path_from(&cargo_manifest_dir, "tests/commonmark-0.30-expected-failures.txt");
    let tests_source_code_path = path_from(&out_dir, "commonmark_tests.rs");

    let source_files = vec![
        &build_script_path,
        &tests_path,
        &expected_failures_path,
    ];
    if !needs_rebuild(&tests_source_code_path, source_files)? {
        return Ok(());
    }

    let mut tests_source_code = fs::File::create(tests_source_code_path)?;
    let tests = read_tests(tests_path)?;
    let expected_failures = read_expected_failures(expected_failures_path)?;

    for test in tests.members() {
        let test_title = format!(
            "{}_{}",
            test["section"].to_string().replace(" ", "_").to_lowercase(),
            test["example"].to_string()
        );
        let annotation = if expected_failures.contains(&test_title) {
            "#[test]\n#[should_panic(expected = \"assertion failed\")]"
        } else {
            "#[test]"
        };
        let markdown = test["markdown"].pretty(0);
        let html = test["html"].pretty(0);

        write!(
            tests_source_code,
            "
{annotation}
fn {test_title}() {{
    assert_eq!(common::render_html({markdown}), {html}, \"source: {{:?}}\", {markdown});
}}"
        )?;
    }

    Ok(())
}

fn path_from(from: &str, to: &str) -> path::PathBuf {
    path::Path::new(from).join(to)
}

fn read_tests(filename: path::PathBuf) -> AnyError<json::JsonValue> {
    Ok(json::parse(
        &String::from_utf8_lossy(&fs::read(filename)?)
    )?)
}

fn read_expected_failures(filename: path::PathBuf) -> AnyError<HashSet<String>> {
    Ok(fs::read_to_string(filename)?
        .lines()
        .map(String::from)
        .collect())
}

fn needs_rebuild(built_path: &path::PathBuf, source_paths: Vec<&path::PathBuf>) -> std::io::Result<bool> {
    let built_modified = match fs::metadata(built_path) {
        Ok(metadata) => metadata.modified()?,
        Err(error) => {
            return match error.kind() {
                std::io::ErrorKind::NotFound => Ok(true),
                _ => Err(error),
            };
        },
    };

    for source_path in source_paths {
        let source_modified = fs::metadata(source_path)?.modified()?;
        if source_modified > built_modified {
            return Ok(true);
        }
    }

    Ok(false)
}
