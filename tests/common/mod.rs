use mdgrep::markdown_events::{CodeBlockKind, ContainerBlock, Delta, Event, LeafBlock, MarkdownEvents};
use mdgrep::block_tokens::ListItemMarkerKind;

pub fn render_html(source: &'static str) -> String {
    let input = std::io::Cursor::new(source);
    let events = MarkdownEvents::new(input);
    let mut html = String::new();

    let mut previous_was_text = false;
    for event in events {
        let mut current_is_text = false;
        let markup = match event {
            Event::ContainerBlock(block, delta) => {
                match (delta, block) {
                    (Delta::Open, ContainerBlock::BlockQuote) => "<blockquote>\n",
                    (Delta::Close, ContainerBlock::BlockQuote) => "</blockquote>\n",
                    (Delta::Open, ContainerBlock::List(marker)) => {
                        match marker.kind {
                            ListItemMarkerKind::Integer(_, _) => "<ol>\n",
                            _ => "<ul>\n",
                        }
                    },
                    (Delta::Close, ContainerBlock::List(marker)) => {
                        match marker.kind {
                            ListItemMarkerKind::Integer(_, _) => "</ol>\n",
                            _ => "</ul>\n",
                        }
                    }
                    (Delta::Open, ContainerBlock::ListItem(_)) => "<li>\n",
                    (Delta::Close, ContainerBlock::ListItem(_)) => "</li>\n",
                    (Delta::Empty, _) => panic!("This is bad news"),
                }.to_string()
            }
            Event::LeafBlock(leaf, delta) => {
                match (delta, leaf) {
                    (Delta::Open, LeafBlock::Code(kind)) => {
                        match kind {
                            CodeBlockKind::Fenced(fence, _) => {
                                if let Some(info) = fence.info {
                                    format!("<pre><code class=\"language-{}\">", info)
                                } else {
                                    "<pre><code>".to_string()
                                }
                            },
                            _ => "<pre><code>".to_string(),
                        }
                    },
                    (Delta::Close, LeafBlock::Code(_)) => "\n</code></pre>\n".to_string(),
                    (Delta::Open, LeafBlock::Paragraph) => "\n<p>".to_string(),
                    (Delta::Close, LeafBlock::Paragraph) => "</p>\n".to_string(),
                    (Delta::Open, LeafBlock::Heading(level)) =>  format!("<h{}>", level),
                    (Delta::Close, LeafBlock::Heading(level)) => format!("</h{}>\n", level),
                    (Delta::Empty, LeafBlock::ThematicBreak) => "<hr />\n".to_string(),
                    (_, _) => panic!("I don't know what this is"),
                }
            }
            Event::Text(_, string) => {
                current_is_text = true;
                if previous_was_text {
                    format!("\n{}", string)
                } else {
                    format!("{}", string)
                }
            }
            Event::SourceLine(_, _) => continue,
        };
        html.push_str(&markup);
        previous_was_text = current_is_text;
    }
    return html
        .trim_start()
        .chars()
        // Collapse adjacent newlines
        .fold(String::new(), |mut memo, next| {
            if next == '\n' && memo.ends_with('\n') {
                memo
            } else {
                memo.push(next);
                memo
            }
        });
}
