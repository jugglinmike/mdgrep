# mdgrep

A command-line utility for finding text in
[CommonMark](https://commonmark.org/)/Markdown-formatted files (i.e. a
Markdown-aware [`grep`](https://www.gnu.org/software/grep/)).

Given a Markdown-formatted file like `post.md`:

    $ cat post.md
    The video game which has occupied my mind the most is Silent Hill
    2. Its story is well-supported by both the gameplay and visuals, and
    it's all just so troubling.

    My favorite video game series:

    1. Silent Hill
    2. Final Fantasy

    Here's what producer Akihiro Imamura had to say about his inspiration:

    > [David Lynch's] style has influenced the series, especially Silent
    > Hill 2. Twin Peaks is one of my personal favourites.

...`mdgrep` can find all the occurrences of a given search term, regardless of
intervening line breaks or formatting tokens:

    $ mdgrep 'Silent Hill 2' post.md
    The video game which has occupied my mind the most is Silent Hill
    2. Its story is well-supported by both the gameplay and visuals, and
    > [David Lynch's] style has influenced the series, especially Silent
    > Hill 2. Twin Peaks is one of my personal favourites.

## Roadmap

- basic support for [CommonMark
  inlines](https://spec.commonmark.org/0.30/#inlines)
- full compliance with [the CommonMark
  specification](https://spec.commonmark.org/)
- CLI: help text (e.g. `-h`/`--help`)
- CLI: context control (e.g. `grep`'s `-A NUM`/`--after-context=NUM`, `-B
  NUM`/`--before-context=NUM`, and `-C NUM`/`--context=NUM`)
- CLI: color control (e.g. `grep`'s `--color[=WHEN]`)
- CLI: accept the standard input stream as input
- CLI: walking directories for input files (e.g. `grep`'s `-r`/`--recursive`)
- CLI: recognize binary files and skip them by default
- regular expression support
- matching diacritics
- filtering by block type (e.g. "only search quoted text", or "ignore text in
  lists")

## License

Copyright 2021 Mike Pennisi under [the GNU General Public License
v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
